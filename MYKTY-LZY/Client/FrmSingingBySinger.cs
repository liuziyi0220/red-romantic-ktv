﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Sunny.UI;

namespace Client
{
    public partial class FrmSingingBySinger : UIForm
    {
        DBHandle dBHandle = new DBHandle();
        int count = -1;
        string imgURL = "";
        int currentPage = 1;
        int totalPage = 0;
        int clickNum = 1;
        public FrmSingingBySinger()
        {
            InitializeComponent();
            //设置各个控件透明
            this.lblLeft.Parent = this.pictureBox2;
            this.lblLeft.Enabled = false;
            this.lblLeft.Visible = false;
            this.lblLeft.BackColor = Color.Transparent;

            this.lblRight.Parent = this.pictureBox2;
            this.lblRight.Enabled = false;
            this.lblRight.Visible = false;
            this.lblRight.BackColor = Color.Transparent;

            this.uiLabel1.Parent = this.pictureBox2;
            this.uiLabel1.BackColor = Color.Transparent;
            this.uiLabel1.ForeColor = Color.White;
      

            this.lblSongsList.Parent = this.pictureBox2;
            this.lblSongsList.BackColor = Color.Transparent;
            this.lblSongsList.ForeColor = Color.White;       

            this.stSongsName.Parent = this.pictureBox2;
            this.stSongsName.BackColor = Color.Transparent;
            this.stSongsName.FillColor = Color.Transparent;
            this.stSongsName.ForeColor = Color.Gold;
            this.stSongsName.RectColor = Color.Transparent;

            this.uiLabel5.Parent = this.pictureBox2;
            this.uiLabel5.BackColor = Color.Transparent;
            this.uiLabel5.ForeColor = Color.White;

            this.lblBackMenu.Parent = this.pictureBox2;
            this.lblBackMenu.BackColor = Color.Transparent;
            this.lblBackMenu.ForeColor = Color.White;

            this.lblUP.Parent = this.pictureBox2;
            this.lblUP.BackColor = Color.Transparent;

            this.lblcontral.Parent = this.pictureBox2;
            this.lblcontral.BackColor = Color.Transparent;
            this.lblcontral.ImageIndex = 0;

            this.lblDown.Parent = this.pictureBox2;
            this.lblDown.BackColor = Color.Transparent;

            this.lblDaLu.Parent = this.pictureBox2;
            this.lblDaLu.ForeColor = Color.White;
            this.lblDaLu.BackColor = Color.Transparent;

            this.lblDongnanya.Parent = this.pictureBox2;
            this.lblDongnanya.ForeColor = Color.White;
            this.lblDongnanya.BackColor = Color.Transparent;

            this.lblRiben.Parent = this.pictureBox2;
            this.lblRiben.ForeColor = Color.White;
            this.lblRiben.BackColor = Color.Transparent;

            this.lblOumei.Parent = this.pictureBox2;
            this.lblOumei.ForeColor = Color.White;
            this.lblOumei.BackColor = Color.Transparent;

            this.lblGangtai.Parent = this.pictureBox2;
            this.lblGangtai.ForeColor = Color.White;
            this.lblGangtai.BackColor = Color.Transparent;

            this.lblAll.Parent = this.pictureBox2;
            this.lblAll.ForeColor = Color.White;
            this.lblAll.BackColor = Color.Transparent;

            this.lblYingLiang.Parent = this.pictureBox2;
            this.lblYingLiang.BackColor = Color.Transparent;

            this.lblChongBo.Parent = this.pictureBox2;
            this.lblChongBo.BackColor = Color.Transparent;

            this.lblHide.ForeColor = Color.White;
            this.tb.Visible =false;

            this.panel1.Parent = this.pictureBox2;
            this.panel1.BackColor = Color.Transparent;

            this.lblPage.Parent = this.pictureBox2;
            this.lblPage.BackColor = Color.Transparent;
            this.lblPage.ForeColor = Color.White;

            this.lblClearAll.Parent = this.panelPlaySongs;
            this.lblClearAll.BackColor = Color.Transparent;
            this.lblClearAll.ForeColor = Color.White;

            this.panelSongsList.AutoScroll = false;
            //方法开始的时候指定播放源
            this.axWindowsMediaPlayer1.Visible = false;
            this.axWindowsMediaPlayer1.URL="";
            this.axWindowsMediaPlayer1.Ctlcontrols.stop();

            //调用取得歌手数量的方法            
        }

        private void lblBackMenu_Click(object sender, EventArgs e)
        {
            this.Close();
            FrmMenu frmMenu = new FrmMenu();
            frmMenu.Show();
        }


        private void lblcontral_Click(object sender, EventArgs e)//媒体控件，控制播放和暂停
        {
            SongList_Click(sender, e);
            if (this.lblcontral.ImageIndex == 0 && !axWindowsMediaPlayer1.URL.Trim().Equals(string.Empty))
            {
                this.lblcontral.ImageIndex = 1;
                this.axWindowsMediaPlayer1.Ctlcontrols.play();
                this.stSongsName.Text = this.axWindowsMediaPlayer1.currentMedia.name;
            }
            else if(this.lblcontral.ImageIndex == 1 && !axWindowsMediaPlayer1.URL.Trim().Equals(string.Empty))
            {
                this.lblcontral.ImageIndex = 0;
                this.axWindowsMediaPlayer1.Ctlcontrols.pause();
            }
            else
            {
                UIMessageTip.ShowOk("请选择一首歌");
            }
        }

        private void Singer_clear(object sender, EventArgs e)//清除容器内的控件
        {
            int mol = count % 12;
            int num = -1;
            if (count < 12)
            {
                num = count;
            }
            else
            {
                num = 12;
            }
            for (int i = 0; i < num; i++)
            {
                panel1.Controls.RemoveAt(0);
            }

        }
        private void Singer_clear_1(object sender, EventArgs e)//清除容器内最后一页的控件
        {
            //如果图片数量不是12的倍数就调用此方法
            //最后一页的数量就是总数量和12的余数
            int num = count % 12;//取得最后一页的图片数量
            for (int i = 0; i < num; i++)
            {
                panel1.Controls.RemoveAt(0);
            }
            num2 = 12;

        }
        int num2 = 12;//成员变量来控制循环次数
        PictureBox[] pb;
        public void CreatSingerPic(int count, string imgURL, int idChoes, int imgURLChoes, int getCount)//在容器内添加图片方法
        {
            int num = 0;//记录picbox数组下标的变量
            string[] url;//存放从数据库内传过来的图片路径
            int[] id = dBHandle.GetSongsID(idChoes, getCount);//存放从数据库内传过来的图片id           

            pb = new PictureBox[count];//picbox数组
            url = dBHandle.GetImgURL(imgURLChoes, getCount);//dbHandle内的返回string数组的方法（取得数据库内的数据并记录在数组中）
            if (count < 12)//如果图片个数小于12
            {
                num = count;//picbox的下标==count
                num2 = count;//循环的次数也==count
            }
            else
            {
                num = 12;
            }
            for (int i = 0; i < num2; i++)
            {     //能取得picbox下标的算式：(currentPage - 1) * num + i
                  //设置宽高
                pb[(currentPage - 1) * num + i] = new System.Windows.Forms.PictureBox();
                pb[(currentPage - 1) * num + i].Width = 140;
                pb[(currentPage - 1) * num + i].Height = 180;
                pb[(currentPage - 1) * num + i].Tag = id[(currentPage - 1) * num + i];
                pb[(currentPage - 1) * num + i].Name = string.Format("{0}Img", ((currentPage - 1) * num + i));
                //设置边框风格
                pb[(currentPage - 1) * num + i].BorderStyle = BorderStyle.FixedSingle;
                pb[(currentPage - 1) * num + i].Cursor = Cursors.Hand;
                //一个页面放12个图片，分成两行，一行放6个
                //判断并设置图片的位置
                if (i <= 5)//这是图片在第一行的情况
                {
                    pb[(currentPage - 1) * num + i].Location = new Point(i * 149 + 100, 13);
                }
                else//这是图片在第二行的情况
                {
                    pb[(currentPage - 1) * num + i].Location = new Point((i - 6) * 149 + 100, 243);
                }
                //设置图片的显示风格
                pb[(currentPage - 1) * num + i].SizeMode = PictureBoxSizeMode.StretchImage;
                //异常判断，如果找不到图片就显示404
                try
                {
                    pb[(currentPage - 1) * num + i].Image = Image.FromFile(url[(currentPage - 1) * num + i]);
                }
                catch (Exception)
                {
                    pb[(currentPage - 1) * num + i].Image = Image.FromFile(@"C:\Users\MSI-CN\Pictures\Saved Pictures\UPCK{5W9JHC]SL0971MKU}P.png");
                }
                //在容器内添加picbox
                panel1.Controls.Add(pb[(currentPage - 1) * num + i]);
                pb[(currentPage - 1) * num + i].Click += new EventHandler(picbox_Click);
            }
            //计算页码
            if (count % num != 0)
            {
                totalPage = (count / num) + 1;
            }
            else
            {
                totalPage = count / num;
            }
            this.lblPage.Text = currentPage + "/" + totalPage;
        }

        private void picbox_Click(object sender, EventArgs e)
        {
            PictureBox pictureBox = (PictureBox)sender;
            singerID = Convert.ToInt32(pictureBox.Tag);
            isShow = true;
            CreatLable();
        }
        private void lblAll_Click(object sender, EventArgs e)//全部歌手
        {
            this.lblAll.ForeColor = Color.Red;
            this.lblDaLu.ForeColor = Color.White;
            this.lblDongnanya.ForeColor = Color.White;
            this.lblGangtai.ForeColor = Color.White;
            this.lblRiben.ForeColor = Color.White;
            this.lblOumei.ForeColor = Color.White;
            currentPage = 1;
            if (pb != null)
            {
                Array.Clear(pb, 0, pb.Length);
            }
            this.panel1.Controls.Clear();
            this.count = dBHandle.GetCount(0);
            CreatSingerPic(count, imgURL, 0, 0, 0);
            num2 = 12;
            this.lblLeft.Visible = true;
            this.lblLeft.Enabled = true;
            this.lblRight.Visible = true;
            this.lblRight.Enabled = true;

        }
        private void lblDaLu_Click(object sender, EventArgs e)//大陆歌手
        {
            this.lblAll.ForeColor = Color.White;
            this.lblDaLu.ForeColor = Color.Red;
            this.lblDongnanya.ForeColor = Color.White;
            this.lblGangtai.ForeColor = Color.White;
            this.lblRiben.ForeColor = Color.White;
            this.lblOumei.ForeColor = Color.White;
            currentPage = 1;
            num2 = 12;
            if (pb != null)
            {
                Array.Clear(pb, 0, pb.Length);
            }
            this.panel1.Controls.Clear();
            this.count = dBHandle.GetCount(1);
            CreatSingerPic(count, imgURL, 1, 1, 1);
            num2 = 12;
            this.lblLeft.Visible = true;
            this.lblLeft.Enabled = true;
            this.lblRight.Visible = true;
            this.lblRight.Enabled = true;
        }

        int singerID;
        string[] songsName;
        string singerName;
        Label[] arrlbl;
        int songsCount = 0;
        int[] songsID;
        string[] songsFileName;
        string clickSongsName;
        public void CreatLable()//点击歌手后出现歌手的歌曲
        {
            songsCount = 0;
            singerName = dBHandle.GetSingerName(singerID);
            songsID = dBHandle.GetSongsID(singerID);
            songsFileName = dBHandle.GetSongsURl(singerID);
            arrlbl = new Label[10];
            songsName = dBHandle.GetSongsName(singerID);
            for (int i = 0; i < 10; i++)
            {
                if (songsName[i] == null)
                {
                    continue;
                }
                songsCount++;
                arrlbl[i] = new Label();               
                arrlbl[i].Text = string.Format("{0} .{1}-------{2}",i+1,songsName[i], singerName);
                arrlbl[i].Name = string.Format("{0}-------{1}",songsName[i], singerName);
                arrlbl[i].BackColor = Color.Transparent;
                arrlbl[i].ForeColor = Color.White;
                arrlbl[i].Parent = panelSongsList;
                arrlbl[i].Width = 250;
                arrlbl[i].Location = new Point(20, i * 35 + 50);
                arrlbl[i].Cursor = Cursors.Hand;
                arrlbl[i].Tag = songsFileName[i];
                panelSongsList.Controls.Add(arrlbl[i]);
                arrlbl[i].Click += new EventHandler(arrLbl_Click);
            }
        }
        private void arrLbl_Click(object sender, EventArgs e)
        {
            Label label = (Label)sender;
            songsClickName = Convert.ToString(label.Name);
            songsClickTag = Convert.ToString(label.Tag);
            UIMessageTip.ShowOk("已添加到播放列表");
            CreatSongsList();
        }

        Label[] songsLbl;
        int songsClick = 0;
        string songsClickName = "";
        string songsClickTag = "";
        string[] songsClickFileName=new string[99];
        int[] songsIndex = new int[99];
        public void CreatSongsList()//在播放列表中添加label
        {
            songsLbl = new Label[99];
            songsLbl[songsClick] = new Label();
            songsLbl[songsClick].Text = (songsClick+1)+".  "+songsClickName;
            songsLbl[songsClick].BackColor = Color.Transparent;
            songsLbl[songsClick].ForeColor = Color.White;
            songsLbl[songsClick].Tag = songsClick;            
            songsLbl[songsClick].Parent = panelPlaySongs;            
            songsLbl[songsClick].Width = 250;                
            songsLbl[songsClick].Cursor = Cursors.Hand;
            songsLbl[songsClick].Location = new Point(50, songsClick * 35 + 50);
            songsLbl[songsClick].Click += new EventHandler(SongList_Click);
            songsClickFileName[songsClick] = songsClickTag;            
            songsClick++;            
            songsClickName = string.Empty;
            songsClickTag = string.Empty;           
        }
        int position;       
        private void SongList_Click(object sender,EventArgs e)
        {
            Label label=(Label)sender;
            this.axWindowsMediaPlayer1.URL = songsClickFileName[Convert.ToInt32(label.Tag)];
            position =Convert.ToInt32(label.Tag);
            this.axWindowsMediaPlayer1.Ctlcontrols.stop();
        }
      
        private void lblGangtai_Click(object sender, EventArgs e)//港台歌手
        {
            this.lblAll.ForeColor = Color.White;
            this.lblDaLu.ForeColor = Color.White;
            this.lblDongnanya.ForeColor = Color.White;
            this.lblGangtai.ForeColor = Color.Red;
            this.lblRiben.ForeColor = Color.White;
            this.lblOumei.ForeColor = Color.White;
            currentPage = 1;
            num2 = 12;
            if (pb != null)
            {
                Array.Clear(pb, 0, pb.Length);
            }
            this.panel1.Controls.Clear();
            this.count = dBHandle.GetCount(2);
            CreatSingerPic(count, imgURL, 2, 2, 2);
            num2 = 12;
            this.lblLeft.Visible = true;
            this.lblLeft.Enabled = true;
            this.lblRight.Visible = true;
            this.lblRight.Enabled = true;
        }


        private void lblLeft_Click(object sender, EventArgs e)//上一页
        {
            if (currentPage == 1)
            {

            }
            else if (currentPage == totalPage && count % 12 != 0)
            {
                clickNum--;
                currentPage--;
                Singer_clear_1(sender, e);
                lblAll_Click(sender, e);
            }
            else
            {
                clickNum--;
                currentPage--;
                Singer_clear(sender, e);
                lblAll_Click(sender, e);
            }
        }

        private void lblRight_Click(object sender, EventArgs e)//下一页
        {
            int forCount = (count / 12) + 1;//页数
            int mol = count % 12;//最后一页的图片个数
            if (currentPage == totalPage)
            {

            }
            else
            {
                clickNum++;//点击下一页的次数
                if (clickNum > forCount)
                {
                    clickNum = forCount;
                }
                if (clickNum == forCount)
                {
                    num2 = mol;
                }
                else
                {
                    num2 = 12;
                }
                currentPage++;
                Singer_clear(sender, e);
                if (pb != null)
                {
                    Array.Clear(pb, 0, pb.Length);
                }
                this.count = dBHandle.GetCount(0);
                CreatSingerPic(count, imgURL, 0, 0, 0);
                num2 = 12;
                this.lblLeft.Visible = true;
                this.lblLeft.Enabled = true;
                this.lblRight.Visible = true;
                this.lblRight.Enabled = true;
            }
        }

        private void lblOumei_Click(object sender, EventArgs e)
        {
            this.lblAll.ForeColor = Color.White;
            this.lblDaLu.ForeColor = Color.White;
            this.lblDongnanya.ForeColor = Color.White;
            this.lblGangtai.ForeColor = Color.White;
            this.lblRiben.ForeColor = Color.White;
            this.lblOumei.ForeColor = Color.Red;
            currentPage = 1;
            num2 = 12;
            if (pb != null)
            {
                Array.Clear(pb, 0, pb.Length);
            }
            this.panel1.Controls.Clear();
            this.count = dBHandle.GetCount(3);
            CreatSingerPic(count, imgURL, 3, 3, 3);
            num2 = 12;
            this.lblLeft.Visible = true;
            this.lblLeft.Enabled = true;
            this.lblRight.Visible = true;
            this.lblRight.Enabled = true;
        }

        
        private void lblRiben_Click(object sender, EventArgs e)
        {
            this.lblAll.ForeColor = Color.White;
            this.lblDaLu.ForeColor = Color.White;
            this.lblDongnanya.ForeColor = Color.White;
            this.lblGangtai.ForeColor = Color.White;
            this.lblRiben.ForeColor = Color.Red;
            this.lblOumei.ForeColor = Color.White;
            currentPage = 1;
            num2 = 12;
            if (pb != null)
            {
                Array.Clear(pb, 0, pb.Length);
            }
            this.panel1.Controls.Clear();
            this.count = dBHandle.GetCount(4);
            CreatSingerPic(count, imgURL, 4, 4, 4);
            num2 = 12;
            this.lblLeft.Visible = true;
            this.lblLeft.Enabled = true;
            this.lblRight.Visible = true;
            this.lblRight.Enabled = true;
        }

        private void lblDongnanya_Click(object sender, EventArgs e)
        {
            this.lblAll.ForeColor = Color.White;
            this.lblDaLu.ForeColor = Color.White;
            this.lblDongnanya.ForeColor = Color.Red;
            this.lblGangtai.ForeColor = Color.White;
            this.lblRiben.ForeColor = Color.White;
            this.lblOumei.ForeColor = Color.White;
            currentPage = 1;
            num2 = 12;
            if (pb != null)
            {
                Array.Clear(pb, 0, pb.Length);
            }
            this.panel1.Controls.Clear();
            this.count = dBHandle.GetCount(5);
            CreatSingerPic(count, imgURL, 5, 5, 5);
            num2 = 12;
            this.lblLeft.Visible = true;
            this.lblLeft.Enabled = true;
            this.lblRight.Visible = true;
            this.lblRight.Enabled = true;
        }

        private void FrmSingingBySinger_Load(object sender, EventArgs e)
        {
            lblAll_Click(sender, e);
        }

        private void timSongsList_Tick(object sender, EventArgs e)
        {
            if (isShow && panelSongsList.Location.X > 930)
            {
                panelSongsList.Location = new Point(panelSongsList.Location.X - 30, panelSongsList.Location.Y);
            }
        }
        bool isShow = false;

        private void lblSongsList_Click(object sender, EventArgs e)
        {
            this.panelPlaySongs.Location = new Point(450, 69);
        }

        private void lblHide_Click(object sender, EventArgs e)
        {
            isShow = false;
            for (int i = 0; i < songsCount; i++)
            {
                panelSongsList.Controls.RemoveAt(1);
            }
        }

        private void timerSongs2_Tick(object sender, EventArgs e)
        {
            if (!isShow && panelSongsList.Location.X < 1780)
            {
                panelSongsList.Location = new Point(panelSongsList.Location.X + 10, panelSongsList.Location.Y);
            }
        }

        private void lblHide2_Click(object sender, EventArgs e)
        {
            this.panelPlaySongs.Location = new Point(1222, 453);
        }
        
        private void lblUP_Click(object sender, EventArgs e)//上一曲
        {
            try
            {
                this.axWindowsMediaPlayer1.URL = Convert.ToString(songsClickFileName[position-1]);
                this.stSongsName.Text = this.axWindowsMediaPlayer1.currentMedia.name;
                position--;
            }
            catch (Exception)
            {
                this.axWindowsMediaPlayer1.URL = Convert.ToString(songsClickFileName[position]);
            }
        }

        private void lblDown_Click(object sender, EventArgs e)//下一曲
        {
            try
            {                
                this.axWindowsMediaPlayer1.URL = Convert.ToString(songsClickFileName[position + 1]);
                this.stSongsName.Text = this.axWindowsMediaPlayer1.currentMedia.name;
                position++;
            }
            catch (Exception)
            {
                this.axWindowsMediaPlayer1.URL = Convert.ToString(songsClickFileName[position]);
            }
        }

        private void lblChongBo_Click(object sender, EventArgs e)
        {
            this.axWindowsMediaPlayer1.URL = Convert.ToString(songsClickFileName[position]);
        }

        private void lblYingLiang_Click(object sender, EventArgs e)
        {
            this.tb.Visible = true;
            axWindowsMediaPlayer1.settings.volume = this.tb.Value;
        }

        private void tb_MouseLeave(object sender, EventArgs e)
        {
            this.tb.Visible = false;
        }

        private void tb_ValueChanged(object sender, EventArgs e)
        {
            axWindowsMediaPlayer1.settings.volume = this.tb.Value;
        }

        private void lblClearAll_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < songsClickFileName.Length - 1; i++)
                {
                    this.panelPlaySongs.Controls.RemoveAt(2);
                }

            }
            catch (Exception)
            {
                
            }
            Array.Clear(songsClickFileName,0,songsClickFileName.Length);
            songsClick = 0;
            this.stSongsName.Text = "";
            UIMessageTip.ShowOk("已清空所有歌曲");
        }
    }
}
