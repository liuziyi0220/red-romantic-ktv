﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Sunny.UI;

namespace Client
{
    public partial class FrmUserRequest : Sunny.UI.UIForm
    {
        public FrmUserRequest()
        {
            InitializeComponent();
            this.uiLabel1.BackColor = Color.Transparent;
            this.uiLabel1.ForeColor = Color.White;
            this.txtbox.Style = UIStyle.White;
            this.uiButton1.Style = UIStyle.White;

        }
        DBHandle dBHandle = new DBHandle();
        public void AddRequest()
        {
            if (!this.txtbox.Text.Trim().Equals(string.Empty))
            {
                dBHandle.AddRequest(this.txtbox.Text);
                this.ShowInfoDialog( "添加成功！",UIStyle.White);
                this.Close();
            }
            else
            {
                this.ShowInfoDialog("输入不能为空！", UIStyle.White);
            }
        }

        private void uiButton1_Click(object sender, EventArgs e)
        {
            AddRequest();
        }

        private void uiLabel2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
