﻿namespace Client
{
    partial class FrmSingingBySinger
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmSingingBySinger));
            this.lblSongsList = new Sunny.UI.UILabel();
            this.uiLabel1 = new Sunny.UI.UILabel();
            this.uiLabel5 = new Sunny.UI.UILabel();
            this.lblBackMenu = new Sunny.UI.UILabel();
            this.imgList = new System.Windows.Forms.ImageList(this.components);
            this.lblcontral = new Sunny.UI.UILabel();
            this.axWindowsMediaPlayer1 = new AxWMPLib.AxWindowsMediaPlayer();
            this.lblDaLu = new Sunny.UI.UILabel();
            this.lblGangtai = new Sunny.UI.UILabel();
            this.lblOumei = new Sunny.UI.UILabel();
            this.lblRiben = new Sunny.UI.UILabel();
            this.lblDongnanya = new Sunny.UI.UILabel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblPage = new Sunny.UI.UILabel();
            this.lblAll = new Sunny.UI.UILabel();
            this.stSongsName = new Sunny.UI.UIScrollingText();
            this.timSongsList1 = new System.Windows.Forms.Timer(this.components);
            this.timerSongs2 = new System.Windows.Forms.Timer(this.components);
            this.tb = new Sunny.UI.UITrackBar();
            this.panelPlaySongs = new System.Windows.Forms.Panel();
            this.lblClearAll = new Sunny.UI.UILabel();
            this.lblHide2 = new Sunny.UI.UILabel();
            this.panelSongsList = new System.Windows.Forms.Panel();
            this.lblHide = new Sunny.UI.UILabel();
            this.lblRight = new Sunny.UI.UILabel();
            this.lblLeft = new Sunny.UI.UILabel();
            this.lblDown = new Sunny.UI.UILabel();
            this.lblChongBo = new Sunny.UI.UILabel();
            this.lblYingLiang = new Sunny.UI.UILabel();
            this.lblUP = new Sunny.UI.UILabel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.axWindowsMediaPlayer1)).BeginInit();
            this.panelPlaySongs.SuspendLayout();
            this.panelSongsList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // lblSongsList
            // 
            this.lblSongsList.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblSongsList.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.lblSongsList.Location = new System.Drawing.Point(782, 35);
            this.lblSongsList.Name = "lblSongsList";
            this.lblSongsList.Size = new System.Drawing.Size(70, 23);
            this.lblSongsList.Style = Sunny.UI.UIStyle.Custom;
            this.lblSongsList.TabIndex = 9;
            this.lblSongsList.Text = "歌单";
            this.lblSongsList.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblSongsList.Click += new System.EventHandler(this.lblSongsList_Click);
            // 
            // uiLabel1
            // 
            this.uiLabel1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel1.Location = new System.Drawing.Point(99, 35);
            this.uiLabel1.Name = "uiLabel1";
            this.uiLabel1.Size = new System.Drawing.Size(115, 23);
            this.uiLabel1.Style = Sunny.UI.UIStyle.Custom;
            this.uiLabel1.TabIndex = 7;
            this.uiLabel1.Text = "正在播放：";
            this.uiLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel5
            // 
            this.uiLabel5.Font = new System.Drawing.Font("微软雅黑", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiLabel5.Location = new System.Drawing.Point(159, 90);
            this.uiLabel5.Name = "uiLabel5";
            this.uiLabel5.Size = new System.Drawing.Size(71, 23);
            this.uiLabel5.Style = Sunny.UI.UIStyle.Custom;
            this.uiLabel5.TabIndex = 4;
            this.uiLabel5.Text = "歌手点歌";
            this.uiLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblBackMenu
            // 
            this.lblBackMenu.Font = new System.Drawing.Font("微软雅黑", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblBackMenu.Location = new System.Drawing.Point(91, 90);
            this.lblBackMenu.Name = "lblBackMenu";
            this.lblBackMenu.Size = new System.Drawing.Size(71, 23);
            this.lblBackMenu.Style = Sunny.UI.UIStyle.Custom;
            this.lblBackMenu.TabIndex = 4;
            this.lblBackMenu.Text = "主菜单>";
            this.lblBackMenu.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblBackMenu.Click += new System.EventHandler(this.lblBackMenu_Click);
            // 
            // imgList
            // 
            this.imgList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imgList.ImageStream")));
            this.imgList.TransparentColor = System.Drawing.Color.Transparent;
            this.imgList.Images.SetKeyName(0, "bofang.png");
            this.imgList.Images.SetKeyName(1, "bofang_1.png");
            // 
            // lblcontral
            // 
            this.lblcontral.BackColor = System.Drawing.Color.Transparent;
            this.lblcontral.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblcontral.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.lblcontral.ImageList = this.imgList;
            this.lblcontral.Location = new System.Drawing.Point(547, 665);
            this.lblcontral.Name = "lblcontral";
            this.lblcontral.Size = new System.Drawing.Size(100, 50);
            this.lblcontral.Style = Sunny.UI.UIStyle.Custom;
            this.lblcontral.TabIndex = 12;
            this.lblcontral.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblcontral.Click += new System.EventHandler(this.lblcontral_Click);
            // 
            // axWindowsMediaPlayer1
            // 
            this.axWindowsMediaPlayer1.Enabled = true;
            this.axWindowsMediaPlayer1.Location = new System.Drawing.Point(354, 497);
            this.axWindowsMediaPlayer1.Name = "axWindowsMediaPlayer1";
            this.axWindowsMediaPlayer1.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axWindowsMediaPlayer1.OcxState")));
            this.axWindowsMediaPlayer1.Size = new System.Drawing.Size(75, 23);
            this.axWindowsMediaPlayer1.TabIndex = 14;
            // 
            // lblDaLu
            // 
            this.lblDaLu.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblDaLu.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.lblDaLu.Location = new System.Drawing.Point(199, 123);
            this.lblDaLu.Name = "lblDaLu";
            this.lblDaLu.Size = new System.Drawing.Size(100, 23);
            this.lblDaLu.Style = Sunny.UI.UIStyle.Custom;
            this.lblDaLu.TabIndex = 15;
            this.lblDaLu.Text = "大陆歌手";
            this.lblDaLu.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblDaLu.Click += new System.EventHandler(this.lblDaLu_Click);
            // 
            // lblGangtai
            // 
            this.lblGangtai.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblGangtai.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.lblGangtai.Location = new System.Drawing.Point(348, 123);
            this.lblGangtai.Name = "lblGangtai";
            this.lblGangtai.Size = new System.Drawing.Size(100, 23);
            this.lblGangtai.Style = Sunny.UI.UIStyle.Custom;
            this.lblGangtai.TabIndex = 15;
            this.lblGangtai.Text = "欧美歌手";
            this.lblGangtai.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblGangtai.Click += new System.EventHandler(this.lblGangtai_Click);
            // 
            // lblOumei
            // 
            this.lblOumei.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblOumei.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.lblOumei.Location = new System.Drawing.Point(519, 123);
            this.lblOumei.Name = "lblOumei";
            this.lblOumei.Size = new System.Drawing.Size(100, 23);
            this.lblOumei.Style = Sunny.UI.UIStyle.Custom;
            this.lblOumei.TabIndex = 15;
            this.lblOumei.Text = "港台歌手";
            this.lblOumei.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblOumei.Click += new System.EventHandler(this.lblOumei_Click);
            // 
            // lblRiben
            // 
            this.lblRiben.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblRiben.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.lblRiben.Location = new System.Drawing.Point(685, 123);
            this.lblRiben.Name = "lblRiben";
            this.lblRiben.Size = new System.Drawing.Size(100, 23);
            this.lblRiben.Style = Sunny.UI.UIStyle.Custom;
            this.lblRiben.TabIndex = 15;
            this.lblRiben.Text = "日本歌手";
            this.lblRiben.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblRiben.Click += new System.EventHandler(this.lblRiben_Click);
            // 
            // lblDongnanya
            // 
            this.lblDongnanya.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblDongnanya.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.lblDongnanya.Location = new System.Drawing.Point(841, 123);
            this.lblDongnanya.Name = "lblDongnanya";
            this.lblDongnanya.Size = new System.Drawing.Size(115, 23);
            this.lblDongnanya.Style = Sunny.UI.UIStyle.Custom;
            this.lblDongnanya.TabIndex = 15;
            this.lblDongnanya.Text = "东南亚歌手";
            this.lblDongnanya.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblDongnanya.Click += new System.EventHandler(this.lblDongnanya_Click);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(72, 166);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1082, 459);
            this.panel1.TabIndex = 16;
            // 
            // lblPage
            // 
            this.lblPage.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.lblPage.Location = new System.Drawing.Point(567, 628);
            this.lblPage.Name = "lblPage";
            this.lblPage.Size = new System.Drawing.Size(100, 23);
            this.lblPage.Style = Sunny.UI.UIStyle.Custom;
            this.lblPage.TabIndex = 0;
            this.lblPage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblAll
            // 
            this.lblAll.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblAll.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.lblAll.Location = new System.Drawing.Point(99, 123);
            this.lblAll.Name = "lblAll";
            this.lblAll.Size = new System.Drawing.Size(53, 23);
            this.lblAll.Style = Sunny.UI.UIStyle.Custom;
            this.lblAll.TabIndex = 15;
            this.lblAll.Text = "全部";
            this.lblAll.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblAll.Click += new System.EventHandler(this.lblAll_Click);
            // 
            // stSongsName
            // 
            this.stSongsName.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(243)))), ((int)(((byte)(255)))));
            this.stSongsName.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.stSongsName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(160)))), ((int)(((byte)(255)))));
            this.stSongsName.Location = new System.Drawing.Point(220, 35);
            this.stSongsName.Name = "stSongsName";
            this.stSongsName.Size = new System.Drawing.Size(191, 29);
            this.stSongsName.Style = Sunny.UI.UIStyle.Custom;
            this.stSongsName.TabIndex = 18;
            // 
            // timSongsList1
            // 
            this.timSongsList1.Enabled = true;
            this.timSongsList1.Interval = 1;
            this.timSongsList1.Tick += new System.EventHandler(this.timSongsList_Tick);
            // 
            // timerSongs2
            // 
            this.timerSongs2.Enabled = true;
            this.timerSongs2.Interval = 10;
            this.timerSongs2.Tick += new System.EventHandler(this.timerSongs2_Tick);
            // 
            // tb
            // 
            this.tb.DisableColor = System.Drawing.Color.Silver;
            this.tb.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.tb.Location = new System.Drawing.Point(310, 628);
            this.tb.Name = "tb";
            this.tb.Size = new System.Drawing.Size(150, 29);
            this.tb.Style = Sunny.UI.UIStyle.Custom;
            this.tb.TabIndex = 21;
            this.tb.Text = "uiTrackBar1";
            this.tb.ValueChanged += new System.EventHandler(this.tb_ValueChanged);
            this.tb.MouseLeave += new System.EventHandler(this.tb_MouseLeave);
            // 
            // panelPlaySongs
            // 
            this.panelPlaySongs.BackgroundImage = global::Client.Properties.Resources.BackImg__20_;
            this.panelPlaySongs.Controls.Add(this.lblClearAll);
            this.panelPlaySongs.Controls.Add(this.lblHide2);
            this.panelPlaySongs.Location = new System.Drawing.Point(1221, 590);
            this.panelPlaySongs.Name = "panelPlaySongs";
            this.panelPlaySongs.Size = new System.Drawing.Size(385, 556);
            this.panelPlaySongs.TabIndex = 20;
            // 
            // lblClearAll
            // 
            this.lblClearAll.BackColor = System.Drawing.Color.Transparent;
            this.lblClearAll.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.lblClearAll.Location = new System.Drawing.Point(13, 10);
            this.lblClearAll.Name = "lblClearAll";
            this.lblClearAll.Size = new System.Drawing.Size(57, 23);
            this.lblClearAll.Style = Sunny.UI.UIStyle.Custom;
            this.lblClearAll.TabIndex = 1;
            this.lblClearAll.Text = "清空";
            this.lblClearAll.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblClearAll.Click += new System.EventHandler(this.lblClearAll_Click);
            // 
            // lblHide2
            // 
            this.lblHide2.BackColor = System.Drawing.Color.Transparent;
            this.lblHide2.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.lblHide2.Image = global::Client.Properties.Resources.guanbi;
            this.lblHide2.Location = new System.Drawing.Point(331, 5);
            this.lblHide2.Name = "lblHide2";
            this.lblHide2.Size = new System.Drawing.Size(51, 49);
            this.lblHide2.Style = Sunny.UI.UIStyle.Custom;
            this.lblHide2.TabIndex = 0;
            this.lblHide2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblHide2.Click += new System.EventHandler(this.lblHide2_Click);
            // 
            // panelSongsList
            // 
            this.panelSongsList.AutoScroll = true;
            this.panelSongsList.BackgroundImage = global::Client.Properties.Resources.BackImg__20_;
            this.panelSongsList.Controls.Add(this.lblHide);
            this.panelSongsList.Location = new System.Drawing.Point(1223, 166);
            this.panelSongsList.Name = "panelSongsList";
            this.panelSongsList.Size = new System.Drawing.Size(296, 541);
            this.panelSongsList.TabIndex = 19;
            // 
            // lblHide
            // 
            this.lblHide.BackColor = System.Drawing.Color.Transparent;
            this.lblHide.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblHide.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.lblHide.Image = global::Client.Properties.Resources.guanbi;
            this.lblHide.Location = new System.Drawing.Point(265, 0);
            this.lblHide.Name = "lblHide";
            this.lblHide.Size = new System.Drawing.Size(27, 28);
            this.lblHide.Style = Sunny.UI.UIStyle.Custom;
            this.lblHide.TabIndex = 19;
            this.lblHide.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblHide.Click += new System.EventHandler(this.lblHide_Click);
            // 
            // lblRight
            // 
            this.lblRight.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblRight.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.lblRight.Image = ((System.Drawing.Image)(resources.GetObject("lblRight.Image")));
            this.lblRight.Location = new System.Drawing.Point(1154, 308);
            this.lblRight.Name = "lblRight";
            this.lblRight.Size = new System.Drawing.Size(54, 130);
            this.lblRight.Style = Sunny.UI.UIStyle.Custom;
            this.lblRight.TabIndex = 17;
            this.lblRight.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblRight.Click += new System.EventHandler(this.lblRight_Click);
            // 
            // lblLeft
            // 
            this.lblLeft.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblLeft.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.lblLeft.Image = ((System.Drawing.Image)(resources.GetObject("lblLeft.Image")));
            this.lblLeft.Location = new System.Drawing.Point(12, 308);
            this.lblLeft.Name = "lblLeft";
            this.lblLeft.Size = new System.Drawing.Size(54, 130);
            this.lblLeft.Style = Sunny.UI.UIStyle.Custom;
            this.lblLeft.TabIndex = 17;
            this.lblLeft.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblLeft.Click += new System.EventHandler(this.lblLeft_Click);
            // 
            // lblDown
            // 
            this.lblDown.BackColor = System.Drawing.Color.Transparent;
            this.lblDown.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblDown.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.lblDown.Image = ((System.Drawing.Image)(resources.GetObject("lblDown.Image")));
            this.lblDown.Location = new System.Drawing.Point(944, 665);
            this.lblDown.Name = "lblDown";
            this.lblDown.Size = new System.Drawing.Size(100, 50);
            this.lblDown.Style = Sunny.UI.UIStyle.Custom;
            this.lblDown.TabIndex = 13;
            this.lblDown.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblDown.Click += new System.EventHandler(this.lblDown_Click);
            // 
            // lblChongBo
            // 
            this.lblChongBo.BackColor = System.Drawing.Color.Transparent;
            this.lblChongBo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblChongBo.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.lblChongBo.Image = global::Client.Properties.Resources.iconset0345;
            this.lblChongBo.Location = new System.Drawing.Point(752, 665);
            this.lblChongBo.Name = "lblChongBo";
            this.lblChongBo.Size = new System.Drawing.Size(100, 50);
            this.lblChongBo.Style = Sunny.UI.UIStyle.Custom;
            this.lblChongBo.TabIndex = 12;
            this.lblChongBo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblChongBo.Click += new System.EventHandler(this.lblChongBo_Click);
            // 
            // lblYingLiang
            // 
            this.lblYingLiang.BackColor = System.Drawing.Color.Transparent;
            this.lblYingLiang.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblYingLiang.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.lblYingLiang.Image = global::Client.Properties.Resources.yinliangda;
            this.lblYingLiang.Location = new System.Drawing.Point(329, 665);
            this.lblYingLiang.Name = "lblYingLiang";
            this.lblYingLiang.Size = new System.Drawing.Size(100, 50);
            this.lblYingLiang.Style = Sunny.UI.UIStyle.Custom;
            this.lblYingLiang.TabIndex = 12;
            this.lblYingLiang.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblYingLiang.Click += new System.EventHandler(this.lblYingLiang_Click);
            // 
            // lblUP
            // 
            this.lblUP.BackColor = System.Drawing.Color.Transparent;
            this.lblUP.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblUP.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.lblUP.Image = ((System.Drawing.Image)(resources.GetObject("lblUP.Image")));
            this.lblUP.Location = new System.Drawing.Point(130, 665);
            this.lblUP.Name = "lblUP";
            this.lblUP.Size = new System.Drawing.Size(100, 50);
            this.lblUP.Style = Sunny.UI.UIStyle.Custom;
            this.lblUP.TabIndex = 11;
            this.lblUP.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblUP.Click += new System.EventHandler(this.lblUP_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox2.Image = global::Client.Properties.Resources.BackImg__20_;
            this.pictureBox2.Location = new System.Drawing.Point(0, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(1220, 724);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 3;
            this.pictureBox2.TabStop = false;
            // 
            // FrmSingingBySinger
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 27F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1220, 724);
            this.Controls.Add(this.tb);
            this.Controls.Add(this.panelPlaySongs);
            this.Controls.Add(this.panelSongsList);
            this.Controls.Add(this.stSongsName);
            this.Controls.Add(this.lblRight);
            this.Controls.Add(this.lblLeft);
            this.Controls.Add(this.lblPage);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.lblDongnanya);
            this.Controls.Add(this.lblRiben);
            this.Controls.Add(this.lblOumei);
            this.Controls.Add(this.lblGangtai);
            this.Controls.Add(this.lblAll);
            this.Controls.Add(this.lblDaLu);
            this.Controls.Add(this.lblDown);
            this.Controls.Add(this.lblChongBo);
            this.Controls.Add(this.lblYingLiang);
            this.Controls.Add(this.lblcontral);
            this.Controls.Add(this.lblUP);
            this.Controls.Add(this.lblSongsList);
            this.Controls.Add(this.lblBackMenu);
            this.Controls.Add(this.uiLabel5);
            this.Controls.Add(this.uiLabel1);
            this.Controls.Add(this.axWindowsMediaPlayer1);
            this.Controls.Add(this.pictureBox2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmSingingBySinger";
            this.Padding = new System.Windows.Forms.Padding(0);
            this.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.ShowTitle = false;
            this.Style = Sunny.UI.UIStyle.Custom;
            this.Text = "";
            this.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.Load += new System.EventHandler(this.FrmSingingBySinger_Load);
            ((System.ComponentModel.ISupportInitialize)(this.axWindowsMediaPlayer1)).EndInit();
            this.panelPlaySongs.ResumeLayout(false);
            this.panelSongsList.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Sunny.UI.UILabel lblSongsList;
        private Sunny.UI.UILabel uiLabel1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private Sunny.UI.UILabel uiLabel5;
        private Sunny.UI.UILabel lblBackMenu;
        private System.Windows.Forms.ImageList imgList;
        private Sunny.UI.UILabel lblUP;
        private Sunny.UI.UILabel lblcontral;
        private Sunny.UI.UILabel lblDown;
        private AxWMPLib.AxWindowsMediaPlayer axWindowsMediaPlayer1;
        private Sunny.UI.UILabel lblDaLu;
        private Sunny.UI.UILabel lblGangtai;
        private Sunny.UI.UILabel lblOumei;
        private Sunny.UI.UILabel lblRiben;
        private Sunny.UI.UILabel lblDongnanya;
        private System.Windows.Forms.Panel panel1;
        private Sunny.UI.UILabel lblPage;
        private Sunny.UI.UILabel lblAll;
        private Sunny.UI.UILabel lblLeft;
        private Sunny.UI.UILabel lblRight;
        private Sunny.UI.UIScrollingText stSongsName;
        private System.Windows.Forms.Timer timSongsList1;
        private System.Windows.Forms.Panel panelSongsList;
        private Sunny.UI.UILabel lblHide;
        private System.Windows.Forms.Timer timerSongs2;
        private System.Windows.Forms.Panel panelPlaySongs;
        private Sunny.UI.UILabel lblHide2;
        private Sunny.UI.UILabel lblYingLiang;
        private Sunny.UI.UILabel lblChongBo;
        private Sunny.UI.UITrackBar tb;
        private Sunny.UI.UILabel lblClearAll;
    }
}