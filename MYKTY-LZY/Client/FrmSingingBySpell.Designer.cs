﻿namespace Client
{
    partial class FrmSingingBySpell
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmSingingBySpell));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.stSongsName = new Sunny.UI.UIScrollingText();
            this.lblSongsList = new Sunny.UI.UILabel();
            this.lblBackMenu = new Sunny.UI.UILabel();
            this.lblPlaying = new Sunny.UI.UILabel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.lblGemingDianGe = new Sunny.UI.UILabel();
            this.btnSelect = new Sunny.UI.UIButton();
            this.panelSelectSongs = new System.Windows.Forms.Panel();
            this.panelPlaySongs = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.axWindowsMediaPlayer1 = new AxWMPLib.AxWindowsMediaPlayer();
            this.lblcontral = new Sunny.UI.UILabel();
            this.imgList = new System.Windows.Forms.ImageList(this.components);
            this.lblYingLiang = new Sunny.UI.UILabel();
            this.lblChongBo = new Sunny.UI.UILabel();
            this.lblUP = new Sunny.UI.UILabel();
            this.lblDown = new Sunny.UI.UILabel();
            this.uiTrackBar1 = new Sunny.UI.UITrackBar();
            this.songsPlayTime = new System.Windows.Forms.Timer(this.components);
            this.uiTextBox1 = new HZH_Controls.Controls.UCTextBoxEx();
            this.lblKeyBoardChang = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.panelPlaySongs.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.axWindowsMediaPlayer1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Client.Properties.Resources.BackImg__20_;
            this.pictureBox1.Location = new System.Drawing.Point(329, 279);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(100, 50);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // stSongsName
            // 
            this.stSongsName.Cursor = System.Windows.Forms.Cursors.Hand;
            this.stSongsName.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(243)))), ((int)(((byte)(255)))));
            this.stSongsName.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.stSongsName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(160)))), ((int)(((byte)(255)))));
            this.stSongsName.Location = new System.Drawing.Point(220, 35);
            this.stSongsName.Name = "stSongsName";
            this.stSongsName.Size = new System.Drawing.Size(191, 29);
            this.stSongsName.Style = Sunny.UI.UIStyle.Custom;
            this.stSongsName.TabIndex = 23;
            // 
            // lblSongsList
            // 
            this.lblSongsList.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblSongsList.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.lblSongsList.Location = new System.Drawing.Point(866, 108);
            this.lblSongsList.Name = "lblSongsList";
            this.lblSongsList.Size = new System.Drawing.Size(167, 23);
            this.lblSongsList.Style = Sunny.UI.UIStyle.Custom;
            this.lblSongsList.TabIndex = 22;
            this.lblSongsList.Text = "播放列表：";
            this.lblSongsList.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblBackMenu
            // 
            this.lblBackMenu.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblBackMenu.Font = new System.Drawing.Font("微软雅黑", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblBackMenu.Location = new System.Drawing.Point(90, 87);
            this.lblBackMenu.Name = "lblBackMenu";
            this.lblBackMenu.Size = new System.Drawing.Size(71, 23);
            this.lblBackMenu.Style = Sunny.UI.UIStyle.Custom;
            this.lblBackMenu.TabIndex = 20;
            this.lblBackMenu.Text = "主菜单>";
            this.lblBackMenu.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblBackMenu.Click += new System.EventHandler(this.lblBackMenu_Click);
            // 
            // lblPlaying
            // 
            this.lblPlaying.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.lblPlaying.Location = new System.Drawing.Point(99, 35);
            this.lblPlaying.Name = "lblPlaying";
            this.lblPlaying.Size = new System.Drawing.Size(115, 23);
            this.lblPlaying.Style = Sunny.UI.UIStyle.Custom;
            this.lblPlaying.TabIndex = 21;
            this.lblPlaying.Text = "正在播放：";
            this.lblPlaying.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox2.Image = global::Client.Properties.Resources.BackImg__20_;
            this.pictureBox2.Location = new System.Drawing.Point(0, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(1220, 724);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 19;
            this.pictureBox2.TabStop = false;
            // 
            // lblGemingDianGe
            // 
            this.lblGemingDianGe.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblGemingDianGe.Font = new System.Drawing.Font("微软雅黑", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblGemingDianGe.Location = new System.Drawing.Point(156, 87);
            this.lblGemingDianGe.Name = "lblGemingDianGe";
            this.lblGemingDianGe.Size = new System.Drawing.Size(71, 23);
            this.lblGemingDianGe.Style = Sunny.UI.UIStyle.Custom;
            this.lblGemingDianGe.TabIndex = 20;
            this.lblGemingDianGe.Text = "歌名点歌";
            this.lblGemingDianGe.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnSelect
            // 
            this.btnSelect.BackColor = System.Drawing.Color.Transparent;
            this.btnSelect.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSelect.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btnSelect.Location = new System.Drawing.Point(733, 96);
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.Radius = 2;
            this.btnSelect.Size = new System.Drawing.Size(100, 35);
            this.btnSelect.Style = Sunny.UI.UIStyle.Custom;
            this.btnSelect.TabIndex = 25;
            this.btnSelect.Text = "搜索";
            this.btnSelect.Click += new System.EventHandler(this.uiButton1_Click);
            // 
            // panelSelectSongs
            // 
            this.panelSelectSongs.AutoScroll = true;
            this.panelSelectSongs.BackgroundImage = global::Client.Properties.Resources.BackImg__20_;
            this.panelSelectSongs.Location = new System.Drawing.Point(28, 149);
            this.panelSelectSongs.Name = "panelSelectSongs";
            this.panelSelectSongs.Size = new System.Drawing.Size(805, 461);
            this.panelSelectSongs.TabIndex = 26;
            // 
            // panelPlaySongs
            // 
            this.panelPlaySongs.AutoScroll = true;
            this.panelPlaySongs.BackgroundImage = global::Client.Properties.Resources.BackImg__20_;
            this.panelPlaySongs.Controls.Add(this.label1);
            this.panelPlaySongs.Location = new System.Drawing.Point(871, 149);
            this.panelPlaySongs.Name = "panelPlaySongs";
            this.panelPlaySongs.Size = new System.Drawing.Size(331, 461);
            this.panelPlaySongs.TabIndex = 27;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label1.ForeColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(14, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 27);
            this.label1.TabIndex = 0;
            this.label1.Text = "清空";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // axWindowsMediaPlayer1
            // 
            this.axWindowsMediaPlayer1.Enabled = true;
            this.axWindowsMediaPlayer1.Location = new System.Drawing.Point(212, 145);
            this.axWindowsMediaPlayer1.Name = "axWindowsMediaPlayer1";
            this.axWindowsMediaPlayer1.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axWindowsMediaPlayer1.OcxState")));
            this.axWindowsMediaPlayer1.Size = new System.Drawing.Size(75, 23);
            this.axWindowsMediaPlayer1.TabIndex = 28;
            // 
            // lblcontral
            // 
            this.lblcontral.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.lblcontral.ImageList = this.imgList;
            this.lblcontral.Location = new System.Drawing.Point(558, 664);
            this.lblcontral.Name = "lblcontral";
            this.lblcontral.Size = new System.Drawing.Size(100, 50);
            this.lblcontral.Style = Sunny.UI.UIStyle.Custom;
            this.lblcontral.TabIndex = 29;
            this.lblcontral.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblcontral.Click += new System.EventHandler(this.lblcontral_Click);
            // 
            // imgList
            // 
            this.imgList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imgList.ImageStream")));
            this.imgList.TransparentColor = System.Drawing.Color.Transparent;
            this.imgList.Images.SetKeyName(0, "bofang.png");
            this.imgList.Images.SetKeyName(1, "bofang_1.png");
            // 
            // lblYingLiang
            // 
            this.lblYingLiang.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.lblYingLiang.Image = global::Client.Properties.Resources.yinliangda;
            this.lblYingLiang.Location = new System.Drawing.Point(391, 665);
            this.lblYingLiang.Name = "lblYingLiang";
            this.lblYingLiang.Size = new System.Drawing.Size(100, 50);
            this.lblYingLiang.Style = Sunny.UI.UIStyle.Custom;
            this.lblYingLiang.TabIndex = 29;
            this.lblYingLiang.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblYingLiang.Click += new System.EventHandler(this.lblYingLiang_Click);
            // 
            // lblChongBo
            // 
            this.lblChongBo.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.lblChongBo.Image = global::Client.Properties.Resources.iconset0345;
            this.lblChongBo.Location = new System.Drawing.Point(733, 665);
            this.lblChongBo.Name = "lblChongBo";
            this.lblChongBo.Size = new System.Drawing.Size(100, 50);
            this.lblChongBo.Style = Sunny.UI.UIStyle.Custom;
            this.lblChongBo.TabIndex = 29;
            this.lblChongBo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblChongBo.Click += new System.EventHandler(this.lblChongBo_Click);
            // 
            // lblUP
            // 
            this.lblUP.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.lblUP.Image = ((System.Drawing.Image)(resources.GetObject("lblUP.Image")));
            this.lblUP.Location = new System.Drawing.Point(215, 665);
            this.lblUP.Name = "lblUP";
            this.lblUP.Size = new System.Drawing.Size(100, 50);
            this.lblUP.Style = Sunny.UI.UIStyle.Custom;
            this.lblUP.TabIndex = 29;
            this.lblUP.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblUP.Click += new System.EventHandler(this.lblUP_Click);
            // 
            // lblDown
            // 
            this.lblDown.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.lblDown.Image = ((System.Drawing.Image)(resources.GetObject("lblDown.Image")));
            this.lblDown.Location = new System.Drawing.Point(902, 665);
            this.lblDown.Name = "lblDown";
            this.lblDown.Size = new System.Drawing.Size(100, 50);
            this.lblDown.Style = Sunny.UI.UIStyle.Custom;
            this.lblDown.TabIndex = 29;
            this.lblDown.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblDown.Click += new System.EventHandler(this.lblDown_Click);
            // 
            // uiTrackBar1
            // 
            this.uiTrackBar1.DisableColor = System.Drawing.Color.Silver;
            this.uiTrackBar1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiTrackBar1.Location = new System.Drawing.Point(365, 633);
            this.uiTrackBar1.Name = "uiTrackBar1";
            this.uiTrackBar1.Size = new System.Drawing.Size(150, 29);
            this.uiTrackBar1.Style = Sunny.UI.UIStyle.Custom;
            this.uiTrackBar1.TabIndex = 30;
            this.uiTrackBar1.Text = "uiTrackBar1";
            this.uiTrackBar1.Value = 50;
            this.uiTrackBar1.Visible = false;
            this.uiTrackBar1.ValueChanged += new System.EventHandler(this.uiTrackBar1_ValueChanged);
            this.uiTrackBar1.MouseLeave += new System.EventHandler(this.uiTrackBar1_MouseLeave);
            // 
            // songsPlayTime
            // 
            this.songsPlayTime.Enabled = true;
            this.songsPlayTime.Interval = 1000;
            this.songsPlayTime.Tick += new System.EventHandler(this.songsPlayTime_Tick);
            // 
            // uiTextBox1
            // 
            this.uiTextBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiTextBox1.ConerRadius = 5;
            this.uiTextBox1.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.uiTextBox1.DecLength = 2;
            this.uiTextBox1.FillColor = System.Drawing.Color.Empty;
            this.uiTextBox1.FocusBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(77)))), ((int)(((byte)(59)))));
            this.uiTextBox1.Font = new System.Drawing.Font("微软雅黑", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.uiTextBox1.InputText = "";
            this.uiTextBox1.InputType = HZH_Controls.TextInputType.NotControl;
            this.uiTextBox1.IsFocusColor = true;
            this.uiTextBox1.IsRadius = true;
            this.uiTextBox1.IsShowClearBtn = true;
            this.uiTextBox1.IsShowKeyboard = true;
            this.uiTextBox1.IsShowRect = true;
            this.uiTextBox1.IsShowSearchBtn = false;
            this.uiTextBox1.KeyBoardType = HZH_Controls.Controls.KeyBoardType.UCKeyBorderAll_EN;
            this.uiTextBox1.Location = new System.Drawing.Point(511, 96);
            this.uiTextBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiTextBox1.MaxValue = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.uiTextBox1.MinValue = new decimal(new int[] {
            1000000,
            0,
            0,
            -2147483648});
            this.uiTextBox1.Name = "uiTextBox1";
            this.uiTextBox1.Padding = new System.Windows.Forms.Padding(5);
            this.uiTextBox1.PasswordChar = '\0';
            this.uiTextBox1.PromptColor = System.Drawing.Color.Gray;
            this.uiTextBox1.PromptFont = new System.Drawing.Font("微软雅黑", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.uiTextBox1.PromptText = "";
            this.uiTextBox1.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(220)))), ((int)(((byte)(220)))));
            this.uiTextBox1.RectWidth = 1;
            this.uiTextBox1.RegexPattern = "";
            this.uiTextBox1.Size = new System.Drawing.Size(211, 35);
            this.uiTextBox1.TabIndex = 31;
            this.uiTextBox1.KeyboardClick += new System.EventHandler(this.uiTextBox1_KeyboardClick);
            // 
            // lblKeyBoardChang
            // 
            this.lblKeyBoardChang.AutoSize = true;
            this.lblKeyBoardChang.BackColor = System.Drawing.Color.Black;
            this.lblKeyBoardChang.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblKeyBoardChang.Enabled = false;
            this.lblKeyBoardChang.ForeColor = System.Drawing.Color.White;
            this.lblKeyBoardChang.Location = new System.Drawing.Point(372, 96);
            this.lblKeyBoardChang.Name = "lblKeyBoardChang";
            this.lblKeyBoardChang.Size = new System.Drawing.Size(132, 27);
            this.lblKeyBoardChang.TabIndex = 32;
            this.lblKeyBoardChang.Text = "切换输入方式";
            this.lblKeyBoardChang.Visible = false;
            this.lblKeyBoardChang.Click += new System.EventHandler(this.lblKeyBoardChang_Click);
            this.lblKeyBoardChang.MouseLeave += new System.EventHandler(this.lblKeyBoardChang_MouseLeave);
            // 
            // FrmSingingBySpell
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 27F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1220, 724);
            this.Controls.Add(this.lblKeyBoardChang);
            this.Controls.Add(this.uiTextBox1);
            this.Controls.Add(this.uiTrackBar1);
            this.Controls.Add(this.lblUP);
            this.Controls.Add(this.lblYingLiang);
            this.Controls.Add(this.lblDown);
            this.Controls.Add(this.lblChongBo);
            this.Controls.Add(this.lblcontral);
            this.Controls.Add(this.panelPlaySongs);
            this.Controls.Add(this.panelSelectSongs);
            this.Controls.Add(this.btnSelect);
            this.Controls.Add(this.stSongsName);
            this.Controls.Add(this.lblSongsList);
            this.Controls.Add(this.lblGemingDianGe);
            this.Controls.Add(this.lblBackMenu);
            this.Controls.Add(this.lblPlaying);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.axWindowsMediaPlayer1);
            this.Name = "FrmSingingBySpell";
            this.Padding = new System.Windows.Forms.Padding(0);
            this.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(219)))), ((int)(((byte)(227)))));
            this.ShowTitle = false;
            this.Style = Sunny.UI.UIStyle.Custom;
            this.Text = "FrmSingingBySpell";
            this.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(219)))), ((int)(((byte)(227)))));
            this.TitleForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(96)))), ((int)(((byte)(98)))), ((int)(((byte)(102)))));
            this.Load += new System.EventHandler(this.FrmSingingBySpell_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.panelPlaySongs.ResumeLayout(false);
            this.panelPlaySongs.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.axWindowsMediaPlayer1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private Sunny.UI.UIScrollingText stSongsName;
        private Sunny.UI.UILabel lblSongsList;
        private Sunny.UI.UILabel lblBackMenu;
        private Sunny.UI.UILabel lblPlaying;
        private System.Windows.Forms.PictureBox pictureBox2;
        private Sunny.UI.UILabel lblGemingDianGe;
        private Sunny.UI.UIButton btnSelect;
        private System.Windows.Forms.Panel panelSelectSongs;
        private System.Windows.Forms.Panel panelPlaySongs;
        private AxWMPLib.AxWindowsMediaPlayer axWindowsMediaPlayer1;
        private Sunny.UI.UILabel lblcontral;
        private Sunny.UI.UILabel lblYingLiang;
        private Sunny.UI.UILabel lblChongBo;
        private Sunny.UI.UILabel lblUP;
        private Sunny.UI.UILabel lblDown;
        private System.Windows.Forms.ImageList imgList;
        private Sunny.UI.UITrackBar uiTrackBar1;
        private System.Windows.Forms.Timer songsPlayTime;
        private HZH_Controls.Controls.UCTextBoxEx uiTextBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblKeyBoardChang;
    }
}