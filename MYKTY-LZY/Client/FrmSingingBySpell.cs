﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HZH_Controls.Controls;
using Sunny.UI;

namespace Client
{
    public partial class FrmSingingBySpell : UIForm
    {
        DBHandle dBHandle = new DBHandle();
        public FrmSingingBySpell()
        {
            InitializeComponent();
            this.lblPlaying.Parent = this.pictureBox2;
            this.lblPlaying.BackColor = Color.Transparent;
            this.lblPlaying.ForeColor = Color.White;

            this.lblSongsList.Parent = this.pictureBox2;
            this.lblSongsList.BackColor = Color.Transparent;
            this.lblSongsList.ForeColor = Color.White;

            this.lblBackMenu.Parent = this.pictureBox2;
            this.lblBackMenu.BackColor = Color.Transparent;
            this.lblBackMenu.ForeColor = Color.White;

            this.lblGemingDianGe.Parent = this.pictureBox2;
            this.lblGemingDianGe.BackColor = Color.Transparent;
            this.lblGemingDianGe.ForeColor = Color.White;


            this.stSongsName.Parent = this.pictureBox2;
            this.stSongsName.BackColor = Color.Transparent;
            this.stSongsName.FillColor = Color.Transparent;
            this.stSongsName.ForeColor = Color.Gold;
            this.stSongsName.RectColor = Color.Transparent;

            this.lblUP.Parent = this.pictureBox2;
            this.lblUP.BackColor = Color.Transparent;

            this.lblcontral.Parent = this.pictureBox2;
            this.lblcontral.BackColor = Color.Transparent;
            this.lblcontral.ImageIndex = 0;

            this.lblDown.Parent = this.pictureBox2;
            this.lblDown.BackColor = Color.Transparent;
            this.lblDown.Enabled = false;


            this.lblYingLiang.Parent = this.pictureBox2;
            this.lblYingLiang.BackColor = Color.Transparent;

            this.lblChongBo.Parent = this.pictureBox2;
            this.lblChongBo.BackColor = Color.Transparent;

            this.uiTrackBar1.Visible = false;
            this.uiTrackBar1.Enabled = false;



            this.axWindowsMediaPlayer1.URL = "";
        }

        private void lblBackMenu_Click(object sender, EventArgs e)
        {
            this.Close();
            FrmMenu frmMenu = new FrmMenu();
            frmMenu.Show();
        }

        string[] songsName;
        string[] songsFileName;
        int songsCount;
        string[] singerName;
        Label[] arrLbl;
        Songs songs = new Songs();
        public void CreatSongsList(string name,string spell,int choes)
        {
            songs = dBHandle.songs(name,spell,choes);
            songsCount = dBHandle.GetCount(6);
            songsName = songs.songsName;            
            songsFileName = songs.songsFileName;
            singerName = songs.singerName;
            arrLbl = new Label[songsCount];
            for (int i = 0; i < arrLbl.Length; i++)
            {
                                arrLbl[i] = new Label();
                if (songsName[i] == null || singerName[i] == null || songsFileName == null)
                {
                    continue;
                }
                int a = songsName[i].Length;

                arrLbl[i].Name = string.Format("{0}—{1}", songsName[i], singerName[i]);
                if (songsName[i].Length < 10)
                {
                    for (int j = 0; j < 10 - a; j++)
                    {
                        songsName[i] = songsName[i] + "—";
                    }

                }
                arrLbl[i].Text = string.Format("{0}—————————————————{1}", songsName[i], singerName[i]);
                arrLbl[i].Width = 550;
                arrLbl[i].Tag = songsFileName[i];
                arrLbl[i].Location = new Point(100, i * 35 + 30);
                arrLbl[i].BackColor = Color.Transparent;
                arrLbl[i].ForeColor = Color.White;
                arrLbl[i].Parent = this.panelSelectSongs;
                arrLbl[i].Cursor = Cursors.Hand;
                panelSelectSongs.Controls.Add(arrLbl[i]);
                arrLbl[i].Click += new EventHandler(arrLbl_Click);
            }
        }
        private void arrLbl_Click(object sender, EventArgs e)
        {
            Label label = (Label)sender;
            songsClickName = Convert.ToString(label.Name);
            songsClickTag = Convert.ToString(label.Tag);
            UIMessageTip.ShowOk("已添加到播放列表");
            CreatSongsPlayList();
        }
        Label[] songsLbl = new Label[99];
        int songsClick = 0;
        string songsClickName = "";
        string songsClickTag = "";
        string[] songsClickFileName = new string[99];
        int[] songsIndex = new int[99];
        public void CreatSongsPlayList()//在播放列表中添加label
        {
            
            songsLbl[songsClick] = new Label();
            songsLbl[songsClick].Text = (songsClick + 1) + ".  " + songsClickName;
            songsLbl[songsClick].BackColor = Color.Transparent;
            songsLbl[songsClick].ForeColor = Color.White;
            songsLbl[songsClick].Tag = songsClick;
            songsLbl[songsClick].Parent = panelPlaySongs;
            songsLbl[songsClick].Width = 250;
            songsLbl[songsClick].Cursor = Cursors.Hand;
            songsLbl[songsClick].Location = new Point(50, songsClick * 35 + 50);
            songsLbl[songsClick].Click += new EventHandler(SongList_Click);
            songsClickFileName[songsClick] = songsClickTag;
            songsClick++;
            songsClickName = string.Empty;
            songsClickTag = string.Empty;
        }
        int position;

        int songsPlayTimes = 0;
        private void songsPlayTime_Tick(object sender, EventArgs e)
        {
            if (isplay)
            {
                songsPlayTimes++;
                if (songsPlayTimes == Convert.ToInt32(this.axWindowsMediaPlayer1.currentMedia.duration))
                {
                    songsPlayTimes = 0;
                    this.axWindowsMediaPlayer1.URL = Convert.ToString(songsClickFileName[position + 1]);
                    for (int i = 0; i < songsClick; i++)
                    {
                        if (songsLbl[i] == null)
                        {
                            continue;
                        }
                        songsLbl[i].ForeColor = Color.White;
                    }
                    songsLbl[position + 1].ForeColor = Color.Red;    
                    this.stSongsName.Text = this.axWindowsMediaPlayer1.currentMedia.name;
                }
            }
        }
        private void SongList_Click(object sender, EventArgs e)
        {
            for (int i=0;i<songsClick;i++)
            {
                if (songsLbl[i] == null)
                {
                    continue;
                }
                songsLbl[i].ForeColor = Color.White;
            }
            Label label = (Label)sender;
            label.ForeColor = Color.Red;
            this.axWindowsMediaPlayer1.URL = songsClickFileName[Convert.ToInt32(label.Tag)];
            position = Convert.ToInt32(label.Tag);
            this.axWindowsMediaPlayer1.Ctlcontrols.stop();
            this.stSongsName.Text = this.axWindowsMediaPlayer1.currentMedia.name;
            this.lblDown.Enabled = true;
        }
        private void FrmSingingBySpell_Load(object sender, EventArgs e)
        {
            CreatSongsList("","",-1);
        }

        private void uiButton1_Click(object sender, EventArgs e)
        {
            try
            {
                int length = Convert.ToInt32(this.uiTextBox1.InputText);

            }
            catch (Exception)
            {
                this.panelSelectSongs.Controls.Clear();
                CreatSongsList(this.uiTextBox1.InputText, this.uiTextBox1.InputText, 999);
            }

        }

        bool isplay=false;
        private void lblcontral_Click(object sender, EventArgs e)
        {
            if (this.lblcontral.ImageIndex == 0 && !axWindowsMediaPlayer1.URL.Trim().Equals(string.Empty))
            {
                this.lblcontral.ImageIndex = 1;
                this.axWindowsMediaPlayer1.Ctlcontrols.play();
                isplay = true;
                this.stSongsName.Text = this.axWindowsMediaPlayer1.currentMedia.name;
            }
            else if (this.lblcontral.ImageIndex == 1 && !axWindowsMediaPlayer1.URL.Trim().Equals(string.Empty))
            {
                this.lblcontral.ImageIndex = 0;
                this.axWindowsMediaPlayer1.Ctlcontrols.pause();
                isplay = false;
            }
            else
            {
                UIMessageTip.ShowOk("请选择一首歌");
            }
        }

        private void lblUP_Click(object sender, EventArgs e)
        {
            try
            {

                this.axWindowsMediaPlayer1.URL = Convert.ToString(songsClickFileName[position - 1]);
                for (int i = 0; i < songsClick; i++)
                {
                    if (songsLbl[i] == null)
                    {
                        continue;
                    }
                    songsLbl[i].ForeColor = Color.White;
                }
                songsLbl[position - 1].ForeColor = Color.Red;
                isplay = true;
                songsPlayTimes = 0;
                this.stSongsName.Text = this.axWindowsMediaPlayer1.currentMedia.name;
                this.lblcontral.ImageIndex = 1;
                position--;
            }
            catch (Exception)
            {
                this.axWindowsMediaPlayer1.URL = Convert.ToString(songsClickFileName[position]);
            }
        }

        private void lblDown_Click(object sender, EventArgs e)
        {
            try
            {                
                this.axWindowsMediaPlayer1.URL = Convert.ToString(songsClickFileName[position + 1]);
                for (int i = 0; i < songsClick; i++)
                {
                    if (songsLbl[i] == null)
                    {
                        continue;
                    }
                    songsLbl[i].ForeColor = Color.White;
                }
                songsLbl[position + 1].ForeColor = Color.Red;
                isplay = true;
                songsPlayTimes = 0;
                this.stSongsName.Text = this.axWindowsMediaPlayer1.currentMedia.name;
                if (this.axWindowsMediaPlayer1.URL.Trim().Equals(string.Empty))
                {
                    this.axWindowsMediaPlayer1.URL = Convert.ToString(songsClickFileName[position]);
                }
                this.lblcontral.ImageIndex = 1;
                position++;
            }
            catch (Exception)
            {
                this.axWindowsMediaPlayer1.URL = Convert.ToString(songsClickFileName[position]);
            }
        }

        private void lblChongBo_Click(object sender, EventArgs e)
        {
            this.axWindowsMediaPlayer1.URL = Convert.ToString(songsClickFileName[position]);
            isplay = true;
        }

        private void uiTrackBar1_MouseLeave(object sender, EventArgs e)
        {
            this.uiTrackBar1.Visible = false;
            this.uiTrackBar1.Enabled = false;
        }

        private void lblYingLiang_Click(object sender, EventArgs e)
        {
            this.uiTrackBar1.Enabled = true;
            this.uiTrackBar1.Visible = true;
        }

        private void uiTrackBar1_ValueChanged(object sender, EventArgs e)
        {
            this.axWindowsMediaPlayer1.settings.volume = uiTrackBar1.Value;
        }

        private void uiTextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void uiTextBox1_Click(object sender, EventArgs e)
        {
            this.uiTextBox1.Text = string.Empty;
        }

        private void label1_Click(object sender, EventArgs e)
        {
            for (int i=0;i<songsClick;i++)
            {
                this.panelPlaySongs.Controls.RemoveAt(1);
            }
            songsClick = 0;
            UIMessageTip.ShowOk("已清空所有歌曲");
            this.stSongsName.Text = string.Empty;
            this.axWindowsMediaPlayer1.URL = "";
            Array.Clear(songsLbl,0,songsLbl.Length);
        }

        private void uiTextBox1_KeyboardClick(object sender, EventArgs e)
        {
            this.lblKeyBoardChang.Visible = true;
            this.lblKeyBoardChang.Enabled = true;
        }
      

        int keyboard = 0;
        private void lblKeyBoardChang_Click(object sender, EventArgs e)
        {
            if (keyboard==0)
            {
                keyboard = 1;
                this.uiTextBox1.KeyBoardType = KeyBoardType.UCKeyBorderHand;
            }
            else
            {
                keyboard = 0;
                this.uiTextBox1.KeyBoardType = KeyBoardType.UCKeyBorderAll_EN;
            }
        }

        private void lblKeyBoardChang_MouseLeave(object sender, EventArgs e)
        {
            this.lblKeyBoardChang.Visible = false;
            this.lblKeyBoardChang.Enabled = false;
        }
    }
}
