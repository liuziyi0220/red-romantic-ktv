﻿namespace Client
{
    partial class FrmHotSongs
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.stSongsName = new Sunny.UI.UIScrollingText();
            this.lblBackMenu = new Sunny.UI.UILabel();
            this.uiLabel5 = new Sunny.UI.UILabel();
            this.uiLabel1 = new Sunny.UI.UILabel();
            this.SuspendLayout();
            // 
            // stSongsName
            // 
            this.stSongsName.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(243)))), ((int)(((byte)(255)))));
            this.stSongsName.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.stSongsName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(160)))), ((int)(((byte)(255)))));
            this.stSongsName.Location = new System.Drawing.Point(166, 35);
            this.stSongsName.Name = "stSongsName";
            this.stSongsName.Size = new System.Drawing.Size(191, 29);
            this.stSongsName.Style = Sunny.UI.UIStyle.Custom;
            this.stSongsName.TabIndex = 22;
            // 
            // lblBackMenu
            // 
            this.lblBackMenu.Font = new System.Drawing.Font("微软雅黑", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblBackMenu.Location = new System.Drawing.Point(37, 90);
            this.lblBackMenu.Name = "lblBackMenu";
            this.lblBackMenu.Size = new System.Drawing.Size(71, 23);
            this.lblBackMenu.Style = Sunny.UI.UIStyle.Custom;
            this.lblBackMenu.TabIndex = 19;
            this.lblBackMenu.Text = "主菜单>";
            this.lblBackMenu.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblBackMenu.Click += new System.EventHandler(this.lblBackMenu_Click);
            // 
            // uiLabel5
            // 
            this.uiLabel5.Font = new System.Drawing.Font("微软雅黑", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiLabel5.Location = new System.Drawing.Point(105, 90);
            this.uiLabel5.Name = "uiLabel5";
            this.uiLabel5.Size = new System.Drawing.Size(71, 23);
            this.uiLabel5.Style = Sunny.UI.UIStyle.Custom;
            this.uiLabel5.TabIndex = 20;
            this.uiLabel5.Text = "热门排行";
            this.uiLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel1
            // 
            this.uiLabel1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel1.Location = new System.Drawing.Point(45, 35);
            this.uiLabel1.Name = "uiLabel1";
            this.uiLabel1.Size = new System.Drawing.Size(115, 23);
            this.uiLabel1.Style = Sunny.UI.UIStyle.Custom;
            this.uiLabel1.TabIndex = 21;
            this.uiLabel1.Text = "正在播放：";
            this.uiLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // FrmHotSongs
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 27F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Client.Properties.Resources.BackImg__20_;
            this.ClientSize = new System.Drawing.Size(1220, 724);
            this.Controls.Add(this.stSongsName);
            this.Controls.Add(this.lblBackMenu);
            this.Controls.Add(this.uiLabel5);
            this.Controls.Add(this.uiLabel1);
            this.Name = "FrmHotSongs";
            this.Padding = new System.Windows.Forms.Padding(0);
            this.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(219)))), ((int)(((byte)(227)))));
            this.ShowTitle = false;
            this.Style = Sunny.UI.UIStyle.Custom;
            this.Text = "5tvtbgrefefrtere";
            this.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(219)))), ((int)(((byte)(227)))));
            this.TitleForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(96)))), ((int)(((byte)(98)))), ((int)(((byte)(102)))));
            this.ResumeLayout(false);

        }

        #endregion

        private Sunny.UI.UIScrollingText stSongsName;
        private Sunny.UI.UILabel lblBackMenu;
        private Sunny.UI.UILabel uiLabel5;
        private Sunny.UI.UILabel uiLabel1;
    }
}