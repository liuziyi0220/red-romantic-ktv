﻿namespace client
{
    partial class FrmChangeDB
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.uiLabel1 = new Sunny.UI.UILabel();
            this.uiLabel2 = new Sunny.UI.UILabel();
            this.DBAddress = new Sunny.UI.UITextBox();
            this.DBPassword = new Sunny.UI.UITextBox();
            this.BtnOk = new Sunny.UI.UIButton();
            this.uiButton2 = new Sunny.UI.UIButton();
            this.uiLabel3 = new Sunny.UI.UILabel();
            this.txtUserName = new Sunny.UI.UITextBox();
            this.SuspendLayout();
            // 
            // uiLabel1
            // 
            this.uiLabel1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel1.Location = new System.Drawing.Point(66, 71);
            this.uiLabel1.Name = "uiLabel1";
            this.uiLabel1.Size = new System.Drawing.Size(157, 36);
            this.uiLabel1.Style = Sunny.UI.UIStyle.Red;
            this.uiLabel1.TabIndex = 0;
            this.uiLabel1.Text = "服务器地址：";
            this.uiLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel2
            // 
            this.uiLabel2.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel2.Location = new System.Drawing.Point(66, 193);
            this.uiLabel2.Name = "uiLabel2";
            this.uiLabel2.Size = new System.Drawing.Size(157, 36);
            this.uiLabel2.Style = Sunny.UI.UIStyle.Red;
            this.uiLabel2.TabIndex = 0;
            this.uiLabel2.Text = "密码：";
            this.uiLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // DBAddress
            // 
            this.DBAddress.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.DBAddress.FillColor = System.Drawing.Color.White;
            this.DBAddress.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.DBAddress.Location = new System.Drawing.Point(230, 73);
            this.DBAddress.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.DBAddress.Maximum = 2147483647D;
            this.DBAddress.Minimum = -2147483648D;
            this.DBAddress.Name = "DBAddress";
            this.DBAddress.Padding = new System.Windows.Forms.Padding(5);
            this.DBAddress.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.DBAddress.Size = new System.Drawing.Size(172, 34);
            this.DBAddress.Style = Sunny.UI.UIStyle.Red;
            this.DBAddress.TabIndex = 1;
            // 
            // DBPassword
            // 
            this.DBPassword.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.DBPassword.FillColor = System.Drawing.Color.White;
            this.DBPassword.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.DBPassword.Location = new System.Drawing.Point(228, 195);
            this.DBPassword.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.DBPassword.Maximum = 2147483647D;
            this.DBPassword.Minimum = -2147483648D;
            this.DBPassword.Name = "DBPassword";
            this.DBPassword.Padding = new System.Windows.Forms.Padding(5);
            this.DBPassword.PasswordChar = '*';
            this.DBPassword.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.DBPassword.Size = new System.Drawing.Size(174, 34);
            this.DBPassword.Style = Sunny.UI.UIStyle.Red;
            this.DBPassword.TabIndex = 1;
            // 
            // BtnOk
            // 
            this.BtnOk.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnOk.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.BtnOk.FillHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(127)))), ((int)(((byte)(128)))));
            this.BtnOk.FillPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(87)))), ((int)(((byte)(89)))));
            this.BtnOk.FillSelectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(87)))), ((int)(((byte)(89)))));
            this.BtnOk.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.BtnOk.Location = new System.Drawing.Point(71, 261);
            this.BtnOk.Name = "BtnOk";
            this.BtnOk.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.BtnOk.RectHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(127)))), ((int)(((byte)(128)))));
            this.BtnOk.RectPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(87)))), ((int)(((byte)(89)))));
            this.BtnOk.RectSelectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(87)))), ((int)(((byte)(89)))));
            this.BtnOk.Size = new System.Drawing.Size(119, 43);
            this.BtnOk.Style = Sunny.UI.UIStyle.Red;
            this.BtnOk.TabIndex = 2;
            this.BtnOk.Text = "确定";
            this.BtnOk.Click += new System.EventHandler(this.BtnOk_Click);
            // 
            // uiButton2
            // 
            this.uiButton2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.uiButton2.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.uiButton2.FillHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(127)))), ((int)(((byte)(128)))));
            this.uiButton2.FillPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(87)))), ((int)(((byte)(89)))));
            this.uiButton2.FillSelectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(87)))), ((int)(((byte)(89)))));
            this.uiButton2.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiButton2.Location = new System.Drawing.Point(281, 261);
            this.uiButton2.Name = "uiButton2";
            this.uiButton2.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.uiButton2.RectHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(127)))), ((int)(((byte)(128)))));
            this.uiButton2.RectPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(87)))), ((int)(((byte)(89)))));
            this.uiButton2.RectSelectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(87)))), ((int)(((byte)(89)))));
            this.uiButton2.Size = new System.Drawing.Size(119, 43);
            this.uiButton2.Style = Sunny.UI.UIStyle.Red;
            this.uiButton2.TabIndex = 2;
            this.uiButton2.Text = "取消";
            this.uiButton2.Click += new System.EventHandler(this.uiButton2_Click);
            // 
            // uiLabel3
            // 
            this.uiLabel3.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel3.Location = new System.Drawing.Point(66, 131);
            this.uiLabel3.Name = "uiLabel3";
            this.uiLabel3.Size = new System.Drawing.Size(157, 36);
            this.uiLabel3.Style = Sunny.UI.UIStyle.Red;
            this.uiLabel3.TabIndex = 0;
            this.uiLabel3.Text = "用户名：";
            this.uiLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtUserName
            // 
            this.txtUserName.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtUserName.FillColor = System.Drawing.Color.White;
            this.txtUserName.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.txtUserName.Location = new System.Drawing.Point(228, 133);
            this.txtUserName.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtUserName.Maximum = 2147483647D;
            this.txtUserName.Minimum = -2147483648D;
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Padding = new System.Windows.Forms.Padding(5);
            this.txtUserName.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.txtUserName.Size = new System.Drawing.Size(174, 34);
            this.txtUserName.Style = Sunny.UI.UIStyle.Red;
            this.txtUserName.TabIndex = 1;
            // 
            // FrmChangeDB
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 27F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(503, 347);
            this.Controls.Add(this.uiButton2);
            this.Controls.Add(this.BtnOk);
            this.Controls.Add(this.txtUserName);
            this.Controls.Add(this.DBPassword);
            this.Controls.Add(this.DBAddress);
            this.Controls.Add(this.uiLabel3);
            this.Controls.Add(this.uiLabel2);
            this.Controls.Add(this.uiLabel1);
            this.Name = "FrmChangeDB";
            this.Padding = new System.Windows.Forms.Padding(0);
            this.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.ShowTitle = false;
            this.Style = Sunny.UI.UIStyle.Red;
            this.Text = "FrmChangeDB";
            this.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.ResumeLayout(false);

        }

        #endregion

        private Sunny.UI.UILabel uiLabel1;
        private Sunny.UI.UILabel uiLabel2;
        private Sunny.UI.UITextBox DBAddress;
        private Sunny.UI.UITextBox DBPassword;
        private Sunny.UI.UIButton BtnOk;
        private Sunny.UI.UIButton uiButton2;
        private Sunny.UI.UILabel uiLabel3;
        private Sunny.UI.UITextBox txtUserName;
    }
}