﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Sunny.UI;

namespace client
{
    public partial class FrmEditSongs : UIForm
    {
        TextToPingYin textToPingYin = new TextToPingYin();//工具类：把文字转换成拼音
        DBHandle dBHandle = new DBHandle();
        SingerInfo singerInfo = new SingerInfo();
        SqlDataAdapter adapter;
        DataSet dataSet = new DataSet();
        DataTable dataTable;
        DataRow row;
        public FrmEditSongs(int id)
        {
            InitializeComponent();
            singerInfo.Id = id;
        }
        private string Mp3URl = string.Empty;
        public void getURl()//取得歌曲路径
        {
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                Mp3URl = this.ofd.FileName;
                this.txtBoxFileName.Text = Mp3URl;
            }
        }
        public void GetSongsType()//取得歌曲类型
        {
            try
            {
                DataSet dsType = new DataSet();
                SqlDataAdapter sqlDataAdapter = dBHandle.SelectInfoBySQl(2);
                sqlDataAdapter.Fill(dsType, "info");
                this.comboxSongsType.DataSource = dsType.Tables["info"];
                this.comboxSongsType.ValueMember = "id";
                this.comboxSongsType.DisplayMember = "TypeName";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public void GetSingerName()//取得歌手姓名
        {
            try
            {
                SqlDataAdapter sqlDataAdapter = dBHandle.SelectInfoBySQl(0);
                DataSet dsSinger = new DataSet();
                sqlDataAdapter.Fill(dsSinger, "info");
                this.comboxSinger.DataSource = dsSinger.Tables["info"];
                this.comboxSinger.ValueMember = "id";
                this.comboxSinger.DisplayMember = "Name";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public void GetSongsInfo()//取得歌手信息
        {
            adapter = dBHandle.SelectInfoBySQl(8, singerInfo);
            adapter.Fill(dataSet, "info");
            dataTable = dataSet.Tables["info"];
            row = dataTable.Rows[0];
            this.txtBoxSongsName.Text = Convert.ToString(row["SongsName"]);
            this.txtBoxSongsPinYin.Text = Convert.ToString(row["Spell"]);
            this.txtBoxSongsLength.Text = Convert.ToString(row["NameLength"]);
            this.txtBoxFileName.Text = Convert.ToString(row["FileName"]);
            this.comboxSinger.SelectedValue = row["Singer"];
            this.comboxSongsType.SelectedValue = row["SongsType"];
        }
        public void UpdateSongsInfo()//更新歌曲信息
        {
            bool result = CheckInput();
            if (result == true)
            {
                row["SongsName"] = this.txtBoxSongsName.Text;
                row["Spell"] = this.txtBoxSongsPinYin.Text;
                row["NameLength"] = this.txtBoxSongsLength.Text;
                row["FileName"] = this.txtBoxFileName.Text;
                row["Singer"] = this.comboxSinger.SelectedValue;
                row["SongsType"] = this.comboxSongsType.SelectedValue;
                dBHandle.sqlCommandBuilder(adapter, dataSet, "info");
                if (dBHandle.editResult > 0)
                {
                    MessageBox.Show("修改成功！");
                    this.Close();
                }
                else
                {
                    MessageBox.Show("修改失败！");
                }
            }
        }
        public void DeleteSongsInfo()
        {
            row["IsDelete"] = 1;            
            dBHandle.sqlCommandBuilder(adapter, dataSet, "info");
            if (dBHandle.editResult > 0)
            {
                dBHandle.UpdateLastUpdateTime(singerInfo.Id, 2);
                MessageBox.Show("删除成功！");
                this.Close();
            }
            else
            {
                MessageBox.Show("删除失败！");
            }
        }
        public bool CheckInput()
        {
            if (this.txtBoxSongsName.Text.Trim().Equals(string.Empty))
            {
                MessageBox.Show("请输入歌曲名！");
                return false;
            }
            else if (this.txtBoxFileName.Text.Equals(string.Empty))
            {
                MessageBox.Show("请选择歌曲文件目录！");
                return false;
            }
            else if (this.comboxSongsType.SelectedIndex < 0)
            {
                MessageBox.Show("请选择歌曲类型！");
                return false;
            }
            else if (this.comboxSinger.SelectedIndex < 0)
            {
                MessageBox.Show("请选择歌手！");
                return false;
            }
            else
            {
                return true;
            }
        }

        private void FrmEditSongs_Load(object sender, EventArgs e)
        {

            GetSongsType();//取得歌曲类型
            GetSingerName();//取得歌手姓名
            GetSongsInfo();
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {

            DialogResult result = MessageBox.Show("确定要修改吗？", "操作提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
            if (result == DialogResult.OK)
            {
                UpdateSongsInfo();
            }
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            getURl();//取得歌曲路径
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("确定要删除吗？", "操作提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
            if (result == DialogResult.OK)
            {
                DeleteSongsInfo();
            }
        }
    }
}
