﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Sunny.UI;

namespace client
{
    public partial class FrmEditSinger : UIForm
    {

        DBHandle dBHandle = new DBHandle();//dbhandle类对象
        SingerInfo singerInfo = new SingerInfo(); //歌手信息类对象   
        SqlDataAdapter sqlData = null;
        DataTable dataTable = null;
        DataRow row1 = null;
        private string imgURL;//照片路径
        public int Singerid;//歌手id      
        public FrmEditSinger()
        {
            InitializeComponent();
        }
        public void getInfo()//取得信息
        {
            singerInfo.Id = Singerid;
            sqlData = dBHandle.SelectInfoBySQl(3, singerInfo);
            sqlData.Fill(dsSingerInfo, "table");
            dataTable = dsSingerInfo.Tables["table"];
            row1 = dataTable.Rows[0];
            this.txtBoxName.Text = Convert.ToString(row1["Name"]);
            int gender = Convert.ToInt32(row1["Gender"]);
            if (gender == 0)
            {
                this.rbtnMan.Checked = true;
            }
            else
            {
                this.rbtnWomen.Checked = true;
            }
            this.comBoxSingerType.SelectedValue = Convert.ToInt32(row1["SingerType"]);
            string URL = Convert.ToString(row1["SingerImg"]);
            try
            {
                this.picBox.Image = Image.FromFile(URL);
            }
            catch (Exception)
            {
                string str = @"C:\Users\MSI-CN\Pictures\Saved Pictures\UPCK{5W9JHC]SL0971MKU}P.png";
                this.picBox.Image = Image.FromFile(str);
            }
            this.txtBoxSingerDescription.Text = Convert.ToString(row1["Singerdescription"]);
        }
        public void getSingerType()//取得歌手类型
        {
            SqlDataAdapter sqlDataAdapter = dBHandle.SelectInfoBySQl(-1);
            sqlDataAdapter.Fill(this.dsCombox, "info");
            this.comBoxSingerType.DataSource = this.dsCombox.Tables["info"];
            this.comBoxSingerType.DisplayMember = "TypeName";
            this.comBoxSingerType.ValueMember = "id";
        }
        public void DeleteSinger()//删除歌手信息
        {
            row1["IsDelete"] = 1;            
            dBHandle.sqlCommandBuilder(sqlData, dsSingerInfo, "table");
            if (dBHandle.editResult > 0)
            {
                dBHandle.UpdateLastUpdateTime(this.Singerid, 1);
                this.ShowInfoDialog("执行成功", UIStyle.Red);
                this.Close();
            }
            else
            {
                this.ShowErrorDialog("执行失败");
            }
        }
        public void UpdateSinger()//编辑歌手信息
        {
            row1["Name"] = this.txtBoxName.Text;
            int Gender = 3;
            if (rbtnMan.Checked)
            {
                Gender = Convert.ToInt32(this.rbtnMan.Tag);
            }
            else if (rbtnWomen.Checked)
            {
                Gender = Convert.ToInt32(this.rbtnWomen.Tag);
            }
            row1["Gender"] = Gender;
            row1["SingerType"] = Convert.ToInt32(this.comBoxSingerType.SelectedValue);
            if (this.imgURL != null)
            {
                row1["SingerImg"] = this.imgURL;
            }
            row1["Singerdescription"] = this.txtBoxSingerDescription.Text;
            dBHandle.sqlCommandBuilder(sqlData, dsSingerInfo, "table");
            if (dBHandle.editResult > 0)
            {
                this.ShowInfoDialog("执行成功",UIStyle.Red);
                this.Close();
            }
            else
            {
                this.ShowInfoDialog("执行失败", UIStyle.Red);
            }


        }
        public void GetImgURL()
        {
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                imgURL = this.ofd.FileName;
                this.picBox.Image = Image.FromFile(imgURL);
            }
        }
        private void btnSave_Click(object sender, EventArgs e)
        {

            bool result = this.ShowAskDialog("确定要修改吗？",UIStyle.Red);
            if (result == true)
            {
                UpdateSinger();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FrmEditSinger_Load(object sender, EventArgs e)
        {
            getSingerType();
            getInfo();
        }

        private void btnImg_Click(object sender, EventArgs e)
        {
            GetImgURL();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {           
            bool result = this.ShowAskDialog("确定删除吗？", UIStyle.Red);
            if (result == true)
            {
                DeleteSinger();
            }
        }
    }
}
