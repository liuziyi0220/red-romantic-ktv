﻿using Sunny.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace client
{
    public partial class FrmLogin : UIForm
    {
        DBHandle dBHandle = new DBHandle();//dbhandle类对象
        MD5Encrypt MD5 = new MD5Encrypt();//MD5加密类对象
        public FrmLogin()
        {
            InitializeComponent();
        }

        string DBAddress;
        string txtUserName;
        string DBPassword;
        public void ChangeAdd()
        {            
        }
        public void Login()
        {
            //非空验证通过
            string md5PassWord = MD5.MD5Encrypt32(this.edtPassword.Text); //调用密码加密方法
            bool result = dBHandle.findUser(this.edtUser.Text, md5PassWord);//调用登录验证方法，把账号和加密后的密码
            if (result == true)//登录结果反馈                                   //作为参数传过去
            {
                this.ShowErrorDialog("操作提示", "登录成功，欢迎您！");                
                FrmMenu frmMenu = new FrmMenu(this.edtUser.Text);
                frmMenu.Show();
                this.Hide();
            }
            else
            {
                this.ShowErrorDialog("用户名或密码错误，登录失败！");
            }
        }
        public void CheckInput()
        {
            if (this.edtUser.Text.Trim().Equals(string.Empty))
            {
                this.ShowErrorDialog("请输入用户名！");              
                this.edtUser.Focus();
            }
            else if (this.edtPassword.Text.Trim().Equals(string.Empty))
            {
                this.ShowErrorDialog("请输入密码！");
                this.edtPassword.Focus();
            }
            else
            {
                Login();
            }
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            CheckInput();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void uiButton1_Click(object sender, EventArgs e)
        {
            FrmChangeDB frmChangeDB = new FrmChangeDB();
            frmChangeDB.Show();
        }
    }
}
