﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace client
{
    class SingerInfo
    {
        private int _id;
        private string _Name;
        private int _Gender;
        private int _SingerType;
        private string _Singerdescription;
        private string _SingerImg;
        private string _LastUpdateTime;
        private string _AdminName;
        private string _UpdateReason;
        private int _IsDelete;

        public string Name { get => _Name; set => _Name = value; }
        public int Gender { get => _Gender; set => _Gender = value; }
        public int SingerType { get => _SingerType; set => _SingerType = value; }
        public string Singerdescription { get => _Singerdescription; set => _Singerdescription = value; }
        public string SingerImg { get => _SingerImg; set => _SingerImg = value; }
        public string LastUpdateTime { get => _LastUpdateTime; set => _LastUpdateTime = value; }
        public string AdminName { get => _AdminName; set => _AdminName = value; }
        public string UpdateReason { get => _UpdateReason; set => _UpdateReason = value; }
        public int IsDelete { get => _IsDelete; set => _IsDelete = value; }
        public int Id { get => _id; set => _id = value; }
    }
}
