﻿namespace client
{
    partial class FrmEditSinger
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.uiLabel1 = new Sunny.UI.UILabel();
            this.txtBoxName = new Sunny.UI.UITextBox();
            this.uiLabel2 = new Sunny.UI.UILabel();
            this.uiPanel1 = new Sunny.UI.UIPanel();
            this.rbtnWomen = new Sunny.UI.UIRadioButton();
            this.rbtnMan = new Sunny.UI.UIRadioButton();
            this.uiLabel3 = new Sunny.UI.UILabel();
            this.comBoxSingerType = new Sunny.UI.UIComboBox();
            this.uiLabel4 = new Sunny.UI.UILabel();
            this.txtBoxSingerDescription = new Sunny.UI.UIRichTextBox();
            this.btnSave = new Sunny.UI.UIButton();
            this.btnCancel = new Sunny.UI.UIButton();
            this.btnDelete = new Sunny.UI.UIButton();
            this.dsCombox = new System.Data.DataSet();
            this.ofd = new System.Windows.Forms.OpenFileDialog();
            this.dsSingerInfo = new System.Data.DataSet();
            this.picBox = new System.Windows.Forms.PictureBox();
            this.btnImg = new Sunny.UI.UIButton();
            this.uiPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dsCombox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsSingerInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBox)).BeginInit();
            this.SuspendLayout();
            // 
            // uiLabel1
            // 
            this.uiLabel1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel1.Location = new System.Drawing.Point(60, 70);
            this.uiLabel1.Name = "uiLabel1";
            this.uiLabel1.Size = new System.Drawing.Size(100, 23);
            this.uiLabel1.Style = Sunny.UI.UIStyle.Red;
            this.uiLabel1.TabIndex = 0;
            this.uiLabel1.Text = "姓名：";
            this.uiLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtBoxName
            // 
            this.txtBoxName.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtBoxName.FillColor = System.Drawing.Color.White;
            this.txtBoxName.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.txtBoxName.Location = new System.Drawing.Point(147, 70);
            this.txtBoxName.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtBoxName.Maximum = 2147483647D;
            this.txtBoxName.Minimum = -2147483648D;
            this.txtBoxName.Name = "txtBoxName";
            this.txtBoxName.Padding = new System.Windows.Forms.Padding(5);
            this.txtBoxName.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.txtBoxName.Size = new System.Drawing.Size(150, 34);
            this.txtBoxName.Style = Sunny.UI.UIStyle.Red;
            this.txtBoxName.TabIndex = 1;
            // 
            // uiLabel2
            // 
            this.uiLabel2.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel2.Location = new System.Drawing.Point(60, 153);
            this.uiLabel2.Name = "uiLabel2";
            this.uiLabel2.Size = new System.Drawing.Size(100, 23);
            this.uiLabel2.Style = Sunny.UI.UIStyle.Red;
            this.uiLabel2.TabIndex = 0;
            this.uiLabel2.Text = "性别：";
            this.uiLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiPanel1
            // 
            this.uiPanel1.Controls.Add(this.rbtnWomen);
            this.uiPanel1.Controls.Add(this.rbtnMan);
            this.uiPanel1.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.uiPanel1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiPanel1.Location = new System.Drawing.Point(147, 135);
            this.uiPanel1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiPanel1.Name = "uiPanel1";
            this.uiPanel1.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.uiPanel1.Size = new System.Drawing.Size(195, 61);
            this.uiPanel1.Style = Sunny.UI.UIStyle.Red;
            this.uiPanel1.TabIndex = 2;
            this.uiPanel1.Text = null;
            // 
            // rbtnWomen
            // 
            this.rbtnWomen.Cursor = System.Windows.Forms.Cursors.Hand;
            this.rbtnWomen.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.rbtnWomen.Location = new System.Drawing.Point(110, 18);
            this.rbtnWomen.Name = "rbtnWomen";
            this.rbtnWomen.Padding = new System.Windows.Forms.Padding(22, 0, 0, 0);
            this.rbtnWomen.RadioButtonColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.rbtnWomen.Size = new System.Drawing.Size(54, 29);
            this.rbtnWomen.Style = Sunny.UI.UIStyle.Red;
            this.rbtnWomen.TabIndex = 0;
            this.rbtnWomen.Tag = "1";
            this.rbtnWomen.Text = "女";
            // 
            // rbtnMan
            // 
            this.rbtnMan.Cursor = System.Windows.Forms.Cursors.Hand;
            this.rbtnMan.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.rbtnMan.Location = new System.Drawing.Point(14, 18);
            this.rbtnMan.Name = "rbtnMan";
            this.rbtnMan.Padding = new System.Windows.Forms.Padding(22, 0, 0, 0);
            this.rbtnMan.RadioButtonColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.rbtnMan.Size = new System.Drawing.Size(54, 29);
            this.rbtnMan.Style = Sunny.UI.UIStyle.Red;
            this.rbtnMan.TabIndex = 0;
            this.rbtnMan.Tag = "0";
            this.rbtnMan.Text = "男";
            // 
            // uiLabel3
            // 
            this.uiLabel3.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel3.Location = new System.Drawing.Point(22, 224);
            this.uiLabel3.Name = "uiLabel3";
            this.uiLabel3.Size = new System.Drawing.Size(114, 23);
            this.uiLabel3.Style = Sunny.UI.UIStyle.Red;
            this.uiLabel3.TabIndex = 0;
            this.uiLabel3.Text = "歌手类型：";
            this.uiLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // comBoxSingerType
            // 
            this.comBoxSingerType.FillColor = System.Drawing.Color.White;
            this.comBoxSingerType.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.comBoxSingerType.Location = new System.Drawing.Point(147, 224);
            this.comBoxSingerType.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.comBoxSingerType.MinimumSize = new System.Drawing.Size(63, 0);
            this.comBoxSingerType.Name = "comBoxSingerType";
            this.comBoxSingerType.Padding = new System.Windows.Forms.Padding(0, 0, 30, 0);
            this.comBoxSingerType.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.comBoxSingerType.Size = new System.Drawing.Size(195, 34);
            this.comBoxSingerType.Style = Sunny.UI.UIStyle.Red;
            this.comBoxSingerType.TabIndex = 3;
            this.comBoxSingerType.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel4
            // 
            this.uiLabel4.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel4.Location = new System.Drawing.Point(22, 308);
            this.uiLabel4.Name = "uiLabel4";
            this.uiLabel4.Size = new System.Drawing.Size(114, 23);
            this.uiLabel4.Style = Sunny.UI.UIStyle.Red;
            this.uiLabel4.TabIndex = 0;
            this.uiLabel4.Text = "歌手描述：";
            this.uiLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtBoxSingerDescription
            // 
            this.txtBoxSingerDescription.AutoWordSelection = true;
            this.txtBoxSingerDescription.FillColor = System.Drawing.Color.White;
            this.txtBoxSingerDescription.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.txtBoxSingerDescription.Location = new System.Drawing.Point(147, 308);
            this.txtBoxSingerDescription.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtBoxSingerDescription.Name = "txtBoxSingerDescription";
            this.txtBoxSingerDescription.Padding = new System.Windows.Forms.Padding(2);
            this.txtBoxSingerDescription.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.txtBoxSingerDescription.Size = new System.Drawing.Size(580, 180);
            this.txtBoxSingerDescription.Style = Sunny.UI.UIStyle.Red;
            this.txtBoxSingerDescription.TabIndex = 4;
            // 
            // btnSave
            // 
            this.btnSave.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSave.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.btnSave.FillHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(127)))), ((int)(((byte)(128)))));
            this.btnSave.FillPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(87)))), ((int)(((byte)(89)))));
            this.btnSave.FillSelectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(87)))), ((int)(((byte)(89)))));
            this.btnSave.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btnSave.Location = new System.Drawing.Point(406, 512);
            this.btnSave.Name = "btnSave";
            this.btnSave.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.btnSave.RectHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(127)))), ((int)(((byte)(128)))));
            this.btnSave.RectPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(87)))), ((int)(((byte)(89)))));
            this.btnSave.RectSelectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(87)))), ((int)(((byte)(89)))));
            this.btnSave.Size = new System.Drawing.Size(100, 35);
            this.btnSave.Style = Sunny.UI.UIStyle.Red;
            this.btnSave.TabIndex = 5;
            this.btnSave.Text = "保存";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCancel.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.btnCancel.FillHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(127)))), ((int)(((byte)(128)))));
            this.btnCancel.FillPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(87)))), ((int)(((byte)(89)))));
            this.btnCancel.FillSelectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(87)))), ((int)(((byte)(89)))));
            this.btnCancel.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btnCancel.Location = new System.Drawing.Point(627, 512);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.btnCancel.RectHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(127)))), ((int)(((byte)(128)))));
            this.btnCancel.RectPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(87)))), ((int)(((byte)(89)))));
            this.btnCancel.RectSelectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(87)))), ((int)(((byte)(89)))));
            this.btnCancel.Size = new System.Drawing.Size(100, 35);
            this.btnCancel.Style = Sunny.UI.UIStyle.Red;
            this.btnCancel.TabIndex = 5;
            this.btnCancel.Text = "取消";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnDelete.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.btnDelete.FillHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(127)))), ((int)(((byte)(128)))));
            this.btnDelete.FillPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(87)))), ((int)(((byte)(89)))));
            this.btnDelete.FillSelectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(87)))), ((int)(((byte)(89)))));
            this.btnDelete.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btnDelete.Location = new System.Drawing.Point(27, 512);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.btnDelete.RectHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(127)))), ((int)(((byte)(128)))));
            this.btnDelete.RectPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(87)))), ((int)(((byte)(89)))));
            this.btnDelete.RectSelectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(87)))), ((int)(((byte)(89)))));
            this.btnDelete.Size = new System.Drawing.Size(133, 35);
            this.btnDelete.Style = Sunny.UI.UIStyle.Red;
            this.btnDelete.TabIndex = 5;
            this.btnDelete.Text = "删除歌手。。";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // dsCombox
            // 
            this.dsCombox.DataSetName = "NewDataSet";
            // 
            // ofd
            // 
            this.ofd.FileName = "openFileDialog1";
            // 
            // dsSingerInfo
            // 
            this.dsSingerInfo.DataSetName = "NewDataSet";
            // 
            // picBox
            // 
            this.picBox.BackColor = System.Drawing.Color.White;
            this.picBox.Location = new System.Drawing.Point(547, 70);
            this.picBox.Name = "picBox";
            this.picBox.Size = new System.Drawing.Size(148, 162);
            this.picBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picBox.TabIndex = 6;
            this.picBox.TabStop = false;
            // 
            // btnImg
            // 
            this.btnImg.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnImg.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.btnImg.FillHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(127)))), ((int)(((byte)(128)))));
            this.btnImg.FillPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(87)))), ((int)(((byte)(89)))));
            this.btnImg.FillSelectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(87)))), ((int)(((byte)(89)))));
            this.btnImg.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btnImg.Location = new System.Drawing.Point(572, 238);
            this.btnImg.Name = "btnImg";
            this.btnImg.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.btnImg.RectHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(127)))), ((int)(((byte)(128)))));
            this.btnImg.RectPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(87)))), ((int)(((byte)(89)))));
            this.btnImg.RectSelectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(87)))), ((int)(((byte)(89)))));
            this.btnImg.Size = new System.Drawing.Size(100, 35);
            this.btnImg.Style = Sunny.UI.UIStyle.Red;
            this.btnImg.TabIndex = 5;
            this.btnImg.Text = "浏览。。";
            this.btnImg.Click += new System.EventHandler(this.btnImg_Click);
            // 
            // FrmEditSinger
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 27F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(818, 582);
            this.Controls.Add(this.picBox);
            this.Controls.Add(this.btnImg);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.txtBoxSingerDescription);
            this.Controls.Add(this.comBoxSingerType);
            this.Controls.Add(this.uiPanel1);
            this.Controls.Add(this.txtBoxName);
            this.Controls.Add(this.uiLabel4);
            this.Controls.Add(this.uiLabel3);
            this.Controls.Add(this.uiLabel2);
            this.Controls.Add(this.uiLabel1);
            this.Name = "FrmEditSinger";
            this.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.Style = Sunny.UI.UIStyle.Red;
            this.Text = "编辑歌手信息";
            this.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.Load += new System.EventHandler(this.FrmEditSinger_Load);
            this.uiPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dsCombox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsSingerInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Sunny.UI.UILabel uiLabel1;
        private Sunny.UI.UITextBox txtBoxName;
        private Sunny.UI.UILabel uiLabel2;
        private Sunny.UI.UIPanel uiPanel1;
        private Sunny.UI.UIRadioButton rbtnMan;
        private Sunny.UI.UIRadioButton rbtnWomen;
        private Sunny.UI.UILabel uiLabel3;
        private Sunny.UI.UIComboBox comBoxSingerType;
        private Sunny.UI.UILabel uiLabel4;
        private Sunny.UI.UIRichTextBox txtBoxSingerDescription;
        private Sunny.UI.UIButton btnSave;
        private Sunny.UI.UIButton btnCancel;
        private Sunny.UI.UIButton btnDelete;
        private System.Data.DataSet dsCombox;
        private System.Windows.Forms.OpenFileDialog ofd;
        private System.Data.DataSet dsSingerInfo;
        private System.Windows.Forms.PictureBox picBox;
        private Sunny.UI.UIButton btnImg;
    }
}