﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using client.Pages;
using Client.Frm;
using Sunny.UI;

namespace client
{
    public partial class FrmMenu : UIAsideHeaderMainFooterFrame
    {
        public FrmMenu(string admin)
        {
            InitializeComponent();
            //设置关联
            Aside.TabControl = MainTabControl;
            Win32.AnimateWindow(this.Handle, 1000, Win32.AW_HOR_POSITIVE);
            //增加页面到Main
            AddPage(new FrmAddSingerInfo(admin), 1001);
            AddPage(new FrmSelectSinger(), 1002);
            AddPage(new FrmSelectSongs(), 1003);
            AddPage(new FrmAddSongs(admin),1004);
            AddPage(new FrmRecycleSinger(), 1005);
            AddPage(new FrmRecycleSongs(), 1006);
            AddPage(new FrmUserDemand(), 1007);
            AddPage(new FrmPay(), 1008);
            //设置Header节点索引
            TreeNode treeNode1 = Aside.CreateNode("歌手管理", 1001);
            TreeNode treeNode2 = Aside.CreateNode("歌曲管理", 1002);
            TreeNode treeNode3 = Aside.CreateNode("资源回收", 1003);
            TreeNode treeNode4 = Aside.CreateNode("客户请求",1007);
            TreeNode treeNode5 = Aside.CreateNode("支持作者",1008);

            Aside.CreateChildNode(treeNode1,"添加歌手",1001);
            Aside.CreateChildNode(treeNode1,"查询歌手", 1002);

            Aside.CreateChildNode(treeNode2, "添加歌曲", 1004);
            Aside.CreateChildNode(treeNode2, "查询歌曲", 1003);

            Aside.CreateChildNode(treeNode3, "恢复歌手", 1005);
            Aside.CreateChildNode(treeNode3, "恢复歌曲", 1006);
            

            Aside.SelectFirst();
        }

      
    }
}
