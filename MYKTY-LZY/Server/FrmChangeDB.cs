﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Sunny.UI;

namespace client
{
    public partial class FrmChangeDB : UIForm
    {
        public FrmChangeDB()
        {
            InitializeComponent();
        }

        public bool CheckInput()
        {
            if (this.DBAddress.Text.Trim().Equals(string.Empty))
            {
                this.ShowErrorDialog("操作提示", "输入不完整！");
                return false;
            }
            else if (this.txtUserName.Text.Trim().Equals(string.Empty))
            {
                this.ShowErrorDialog("操作提示", "输入不完整！");
                return false;
            }
            else if (this.DBPassword.Text.Trim().Equals(string.Empty))
            {
                this.ShowErrorDialog("操作提示", "输入不完整！");
                return false;
            }
            else
            {
                return true;
            }
        }
        private void BtnOk_Click(object sender, EventArgs e)
        {
            bool result = CheckInput();
            if (result)
            {
                DBHandle.URL = string.Format("Data Source={0};Initial Catalog=RedRomanticKTV;User ID={1};Password={2}",this.DBAddress.Text,this.txtUserName.Text,this.DBPassword.Text);
                this.ShowErrorDialog("操作提示", "设置成功！");
                this.Close();

            }           
        }

        private void uiButton2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
