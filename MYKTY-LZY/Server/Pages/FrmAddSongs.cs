﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using Sunny.UI;

namespace client.Pages
{

    public partial class FrmAddSongs : UIPage
    {
        DBHandle dBHandle = new DBHandle();//DB工具类
        TextToPingYin textToPingYin = new TextToPingYin();//工具类：把文字转换成拼音
        SongsInfo songsInfo = new SongsInfo();//歌曲信息类，用来存放歌曲的字段
        private string AdminName;
        public FrmAddSongs(string adminName)
        {
            InitializeComponent();
            this.AdminName = adminName;
        }
        private string Mp3URl = string.Empty;

        public void getURl()//取得歌曲路径
        {
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                Mp3URl = this.ofd.FileName;
                this.txtBoxFileName.Text = Mp3URl;
            }
        }
        public string GetNamePinYin()//取得歌曲拼音
        {
            string pinyin = textToPingYin.GetPinyin(this.txtBoxSongsName.Text);
            return pinyin;
        }
        public void GetSongsType()//取得歌曲类型
        {
            try
            {
                SqlDataAdapter sqlDataAdapter = dBHandle.SelectInfoBySQl(2);
                dsType = new DataSet();
                sqlDataAdapter.Fill(dsType, "info");
                this.comboxSongsType.DataSource = dsType.Tables["info"];
                this.comboxSongsType.ValueMember = "id";
                this.comboxSongsType.DisplayMember = "TypeName";
            }
            catch (Exception ex)
            {
                this.ShowErrorDialog(ex.Message);
            }
        }
        public void GetSingerName()//取得歌手姓名
        {
            try
            {
                SqlDataAdapter sqlDataAdapter = dBHandle.SelectInfoBySQl(0);
                dsType = new DataSet();
                sqlDataAdapter.Fill(dsSinger, "info");
                this.comboxSinger.DataSource = dsSinger.Tables["info"];
                this.comboxSinger.ValueMember = "id";
                this.comboxSinger.DisplayMember = "Name";
            }
            catch (Exception ex)
            {
                this.ShowErrorDialog(ex.Message);
            }
        }
        public void AddSongs()//添加歌曲
        {
            if (this.txtBoxSongsName.Text.Trim().Equals(string.Empty))
            {
                this.ShowErrorDialog("请输入歌曲名！");
            }
            else if (this.Mp3URl.Equals(string.Empty))
            {
                this.ShowErrorDialog("请选择歌曲文件目录！");
            }
            else if (this.comboxSongsType.SelectedIndex < 0)
            {
                this.ShowErrorDialog("请选择歌曲类型！");
            }
            else if (this.comboxSinger.SelectedIndex < 0)
            {
                this.ShowErrorDialog("请选择歌手！");
            }
            else
            {
                songsInfo.SongsName = this.txtBoxSongsName.Text;
                songsInfo.Spell = this.txtBoxSongsPinYin.Text;
                songsInfo.NameLength = this.txtBoxSongsName.Text.Length;
                songsInfo.SongsType = Convert.ToInt32(this.comboxSongsType.SelectedValue);
                songsInfo.Singer = Convert.ToInt32(this.comboxSinger.SelectedValue);
                songsInfo.FileName = this.Mp3URl;
                songsInfo.PlayNum = 0;
                songsInfo.LastUpdateTime = "GETDATE()";
                songsInfo.AdminName = this.AdminName;
                songsInfo.UpdateReason = "通过管理员账户添加";
                songsInfo.IsDelete = 0;
                bool result = dBHandle.AddSongs(songsInfo);
                if (result == true)
                {
                    this.ShowSuccessDialog("添加成功！", UIStyle.Red);
                }
                else
                {
                    this.ShowErrorDialog("添加失败！");
                }
            }
        }
        public void claer()
        {
            this.txtBoxSongsName.Clear();
            this.txtBoxSongsLength.Clear();
            this.comboxSinger.SelectedIndex = -1;
            this.comboxSongsType.SelectedIndex = -1;
            this.txtBoxFileName.Clear();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            AddSongs();//添加歌曲
        }

        private void FrmAddSongs_Load(object sender, EventArgs e)
        {
            GetSongsType();//取得歌曲类型
            GetSingerName();//取得歌手姓名              
            claer();
        }

        private void comboxSongsType_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.txtBoxSongsPinYin.Text = GetNamePinYin();
            this.txtBoxSongsLength.Text = Convert.ToString(this.txtBoxSongsName.Text.Length);
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            claer();
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            getURl();
        }
    }
}
