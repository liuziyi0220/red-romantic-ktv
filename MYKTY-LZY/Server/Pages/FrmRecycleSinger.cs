﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

using Sunny.UI;

namespace client.Pages
{
    public partial class FrmRecycleSinger : UIPage
    {
        public FrmRecycleSinger()
        {
            InitializeComponent();
        }

        DBHandle dBHandle = new DBHandle();
        SingerInfo singerInfo = new SingerInfo();

        public void getSingerType()//取得歌手类型
        {
            SqlDataAdapter sqlDataAdapter = dBHandle.SelectInfoBySQl(-1);
            sqlDataAdapter.Fill(this.dsCombox, "info");
            this.comBoxSingerType.DataSource = this.dsCombox.Tables["info"];
            this.comBoxSingerType.DisplayMember = "TypeName";
            this.comBoxSingerType.ValueMember = "id";
        }
        public void clear()
        {
            this.txtBoxName.Text = null;
            this.comBoxSingerType.SelectedIndex = -1;

        }
        SqlDataAdapter sqlDataAdapter1 = null;
        public void GetSingerList()//取得歌手列表
        {

            if (this.txtBoxName.Text.Trim().Equals(string.Empty) && this.comBoxSingerType.SelectedIndex < 0)
            {
                try
                {
                    sqlDataAdapter1 = dBHandle.SelectInfoBySQl(9);
                    dsDgv = new DataSet();
                    sqlDataAdapter1.Fill(this.dsDgv, "info");
                    this.dataGridView1.DataSource = this.dsDgv.Tables["info"];
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            else
            {
                try
                {
                    singerInfo.Name = this.txtBoxName.Text;
                    singerInfo.SingerType = Convert.ToInt32(this.comBoxSingerType.SelectedValue);
                    sqlDataAdapter1 = dBHandle.SelectInfoBySQl(12, singerInfo);
                    dsDgv = new DataSet();
                    sqlDataAdapter1.Fill(this.dsDgv, "info");
                    this.dataGridView1.DataSource = this.dsDgv.Tables["info"];
                }
                catch (Exception ex)
                {
                    this.ShowInfoDialog(ex.Message);
                }
            }
        }

        private void FrmRecycleSinger_Load(object sender, EventArgs e)
        {
            this.dataGridView1.AutoGenerateColumns = false;
            dBHandle.DeleteById(2);
            GetSingerList();//取得歌手列表
            getSingerType();//取得歌手类型           
            clear();
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            GetSingerList();//取得歌手列表
        }
        public void RecycleSinger()
        {
            int id = Convert.ToInt32(this.dataGridView1.SelectedCells[5].Value);
            bool result = dBHandle.RecycleSongs(2, id);
            if (result == true)
            {
                this.ShowInfoDialog("恢复成功！", UIStyle.Red);
                GetSingerList();
            }
            else
            {
                this.ShowInfoDialog("恢复失败！", UIStyle.Red);
            }

        }

        private void 恢复ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.dataGridView1.SelectedRows.Count > 0)
                {
                    RecycleSinger();
                }
                else
                {
                    this.ShowInfoDialog("请选中一行！", UIStyle.Red);
                }
            }
            catch (Exception)
            {
                this.ShowInfoDialog("请选中一行！",UIStyle.Red);
            }
        }
    }
}
