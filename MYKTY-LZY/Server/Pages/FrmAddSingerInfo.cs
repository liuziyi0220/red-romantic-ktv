﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Windows.Forms;
using Sunny.UI;

namespace client.Pages
{
    public partial class FrmAddSingerInfo : UIPage
    {
        DBHandle dBHandle = new DBHandle();//dbhandle类对象
        SingerInfo singerInfo = new SingerInfo(); //歌手信息类对象   
        private string adminName;
        public FrmAddSingerInfo(string adminName)
        {
            InitializeComponent();
        }
        public string imgURL;
        public void GetImgURL()
        {
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                imgURL = this.ofd.FileName;
                this.picBox.Image = Image.FromFile(imgURL);
            }
        }

        public void getSingerType()//取得歌手类型
        {
            SqlDataAdapter sqlDataAdapter = dBHandle.SelectInfoBySQl(-1);
            sqlDataAdapter.Fill(this.dsCombox, "info");
            this.comBoxSingerType.DataSource = this.dsCombox.Tables["info"];
            this.comBoxSingerType.DisplayMember = "TypeName";
            this.comBoxSingerType.ValueMember = "id";
        }
        public void addSingerInfo()//添加歌手信息
        {
            int Gender = -1;
            //给性别赋值
            if (rbtnMan.Checked)
            {
                Gender = Convert.ToInt32(this.rbtnMan.Tag);
            }
            else if (rbtnWomen.Checked)
            {
                Gender = Convert.ToInt32(this.rbtnWomen.Tag);
            }
            //非空验证
            if (this.txtBoxSingerName.Text.Trim().Equals(string.Empty))
            {
                this.ShowErrorDialog("请输入歌手姓名！");
            }
            else if (imgURL == null)
            {
                this.ShowErrorDialog("请选择歌手照片！");
            }
            else if (Convert.ToInt32(this.comBoxSingerType.SelectedValue) < 0)
            {
                this.ShowErrorDialog("请选择歌手类型！");
            }

            else if (Gender == -1)
            {
                this.ShowErrorDialog("请选择歌手性别！");
            }
            else
            {
                //非空验证通过后进行赋值
                singerInfo.Name = this.txtBoxSingerName.Text;
                singerInfo.Gender = Gender;
                singerInfo.SingerType = Convert.ToInt32(this.comBoxSingerType.SelectedValue);
                singerInfo.Singerdescription = this.txtBoxSingerDescription.Text;
                singerInfo.SingerImg = imgURL;
                singerInfo.LastUpdateTime = "GETDATE()";
                singerInfo.AdminName = this.adminName;
                singerInfo.UpdateReason = "通过管理员账户添加";
                singerInfo.IsDelete = 0;
                //赋值完成后调用后把对象传给AddSinger方法
                bool result = dBHandle.AddSinger(singerInfo);
                //根据AddSinger方法的返回值进行判断
                if (result == true)
                {
                    this.ShowSuccessDialog("添加成功！",UIStyle.Red);
                }
                else
                {
                    this.ShowErrorDialog("添加失败！");
                }

            }
        }

        private void btnImg_Click(object sender, EventArgs e)
        {
            GetImgURL();
        }

        private void FrmAddSingerInfo_Initialize(object sender, EventArgs e)
        {

        }

        private void FrmAddSingerInfo_Load(object sender, EventArgs e)
        {
            getSingerType();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.txtBoxSingerDescription.Text = string.Empty;
            this.txtBoxSingerName.Text = string.Empty;
            this.picBox.Image = null;
            this.comBoxSingerType.SelectedIndex = -1;
            this.rbtnMan.Checked = false;
            this.rbtnWomen.Checked = false;
            
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            addSingerInfo();
            this.txtBoxSingerDescription.Text = string.Empty;
            this.txtBoxSingerName.Text = string.Empty;
            this.picBox.Image = null;
            this.comBoxSingerType.SelectedIndex = -1;
            this.rbtnMan.Checked = false;
            this.rbtnWomen.Checked = false;
        }
    }
}
