﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using Sunny.UI;

namespace client.Pages
{
    public partial class FrmSelectSinger : UIPage
    {
        DBHandle dBHandle = new DBHandle();
        SingerInfo singerInfo = new SingerInfo();
        public FrmSelectSinger()
        {
            InitializeComponent();
        }
        public void getSingerType()//取得歌手类型
        {
            SqlDataAdapter sqlDataAdapter = dBHandle.SelectInfoBySQl(-1);
            sqlDataAdapter.Fill(this.dsCombox, "info");
            this.comBoxSingerType.DataSource = this.dsCombox.Tables["info"];
            this.comBoxSingerType.DisplayMember = "TypeName";
            this.comBoxSingerType.ValueMember = "id";
        }
        public void clear()
        {
            this.txtBoxName.Text = null;
            this.comBoxSingerType.SelectedIndex = -1;

        }
        SqlDataAdapter sqlDataAdapter1 = null;
        public void GetSingerList()//取得歌手列表
        {

            if (this.txtBoxName.Text.Trim().Equals(string.Empty) && this.comBoxSingerType.Text.Trim().Equals(string.Empty))
            {
                try
                {
                    sqlDataAdapter1 = dBHandle.SelectInfoBySQl(1);
                    dsDgv = new DataSet();
                    sqlDataAdapter1.Fill(this.dsDgv, "info");
                    this.dataGridView1.DataSource = this.dsDgv.Tables["info"];
                }
                catch (Exception ex)
                {
                    this.ShowErrorDialog(ex.Message);
                }
            }
            else
            {
                try
                {
                    singerInfo.Name = this.txtBoxName.Text;
                    singerInfo.SingerType = Convert.ToInt32(this.comBoxSingerType.SelectedValue);
                    sqlDataAdapter1 = dBHandle.SelectInfoBySQl(1, singerInfo);
                    dsDgv = new DataSet();
                    sqlDataAdapter1.Fill(this.dsDgv, "info");
                    this.dataGridView1.DataSource = this.dsDgv.Tables["info"];
                }
                catch (Exception ex)
                {
                    this.ShowErrorDialog(ex.Message);
                }
            }
        }

        public void DeleteSinger()
        {
            dBHandle.sqlCommandBuilder(sqlDataAdapter1, dsDgv, "info");
            if (dBHandle.editResult > 0)
            {
                this.ShowErrorDialog("更新成功！");
            }
        }
        private void FrmSelectSinger_Load(object sender, EventArgs e)
        {
            GetSingerList();//取得歌手列表
            getSingerType();//取得歌手类型           
            clear();
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            GetSingerList();//取得歌手列表
        }

        private void 编辑ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.dataGridView1.SelectedRows.Count > 0)
                {
                    FrmEditSinger frmEditSinger = new FrmEditSinger();
                    frmEditSinger.Singerid = Convert.ToInt32(this.dataGridView1.SelectedCells[5].Value);
                    frmEditSinger.Show();
                }
                else
                {
                  this.ShowErrorDialog("请选中一行！");
                }
            }
            catch (Exception ex)
            {
                this.ShowErrorDialog("请选中一行！");
            }
        }

        private void FrmSelectSinger_Load_1(object sender, EventArgs e)
        {
            this.dataGridView1.AutoGenerateColumns = false;
            GetSingerList();//取得歌手列表
            getSingerType();//取得歌手类型           
            clear();
        }
    }
}
