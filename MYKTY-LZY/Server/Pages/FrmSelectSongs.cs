﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Sunny.UI;

namespace client.Pages
{
    public partial class FrmSelectSongs : UIPage
    {
        DBHandle dBHandle = new DBHandle();
        SingerInfo singerInfo = new SingerInfo();
        public FrmSelectSongs()
        {
            InitializeComponent();
        }
        public void SelectSongsInfo()//查询歌手信息
        {

            if (this.txtBoxName.Text.Trim().Equals("歌曲名/歌手") || this.txtBoxName.Text.Trim().Equals(string.Empty))
            {
                SqlDataAdapter adapter = dBHandle.SelectInfoBySQl(7);
                dsDgv = new DataSet();
                adapter.Fill(dsDgv, "info");
                this.dataGridView1.DataSource = dsDgv.Tables["info"];
            }
            else
            {
                singerInfo.Name = (this.txtBoxName.Text).ToUpper();
                SqlDataAdapter adapter1 = dBHandle.SelectInfoBySQl(4, singerInfo);
                SqlDataAdapter adapter2 = dBHandle.SelectInfoBySQl(5, singerInfo);
                SqlDataAdapter adapter3 = dBHandle.SelectInfoBySQl(6, singerInfo);
                DataSet dsDgv1 = new DataSet();
                adapter1.Fill(dsDgv1, "info");
                adapter2.Fill(dsDgv1, "info");
                adapter3.Fill(dsDgv1, "info");
                this.dataGridView1.DataSource = dsDgv1.Tables["info"];
            }
        }

        private void FrmSelectSongs_Load(object sender, EventArgs e)
        {
            this.dataGridView1.AutoGenerateColumns = false;
            SelectSongsInfo();
        }

        private void txtBoxName_Click(object sender, EventArgs e)
        {
            if (this.txtBoxName.Text.Equals("歌曲名/歌手"))
            {
                this.txtBoxName.Text = string.Empty;
            }
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            SelectSongsInfo();
        }

        private void 编辑ToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
           
            try
            {
                if (this.dataGridView1.SelectedRows.Count > 0)
                {
                    FrmEditSongs frmEditSongs = new FrmEditSongs(Convert.ToInt32(this.dataGridView1.SelectedCells[7].Value));
                    frmEditSongs.Show();
                }
                else
                {
                    this.ShowErrorDialog("请选中一行！");
                }
            }
            catch (Exception)
            {
                this.ShowErrorDialog("请选中一行！");
            }
        }
    }
}
