﻿namespace client.Pages
{
    partial class FrmAddSingerInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtBoxSingerName = new Sunny.UI.UITextBox();
            this.uiLabel1 = new Sunny.UI.UILabel();
            this.uiLabel2 = new Sunny.UI.UILabel();
            this.uiLabel3 = new Sunny.UI.UILabel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.rbtnWomen = new Sunny.UI.UIRadioButton();
            this.rbtnMan = new Sunny.UI.UIRadioButton();
            this.uiLabel4 = new Sunny.UI.UILabel();
            this.comBoxSingerType = new Sunny.UI.UIComboBox();
            this.uiLabel5 = new Sunny.UI.UILabel();
            this.txtBoxSingerDescription = new Sunny.UI.UIRichTextBox();
            this.picBox = new System.Windows.Forms.PictureBox();
            this.btnImg = new Sunny.UI.UIButton();
            this.btnSave = new Sunny.UI.UIButton();
            this.btnCancel = new Sunny.UI.UIButton();
            this.ofd = new System.Windows.Forms.OpenFileDialog();
            this.dsCombox = new System.Data.DataSet();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsCombox)).BeginInit();
            this.SuspendLayout();
            // 
            // txtBoxSingerName
            // 
            this.txtBoxSingerName.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtBoxSingerName.FillColor = System.Drawing.Color.White;
            this.txtBoxSingerName.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.txtBoxSingerName.Location = new System.Drawing.Point(160, 32);
            this.txtBoxSingerName.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtBoxSingerName.Maximum = 2147483647D;
            this.txtBoxSingerName.Minimum = -2147483648D;
            this.txtBoxSingerName.Name = "txtBoxSingerName";
            this.txtBoxSingerName.Padding = new System.Windows.Forms.Padding(5);
            this.txtBoxSingerName.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.txtBoxSingerName.Size = new System.Drawing.Size(217, 34);
            this.txtBoxSingerName.Style = Sunny.UI.UIStyle.Red;
            this.txtBoxSingerName.TabIndex = 0;
            // 
            // uiLabel1
            // 
            this.uiLabel1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel1.Location = new System.Drawing.Point(37, 32);
            this.uiLabel1.Name = "uiLabel1";
            this.uiLabel1.Size = new System.Drawing.Size(0, 0);
            this.uiLabel1.Style = Sunny.UI.UIStyle.Red;
            this.uiLabel1.TabIndex = 1;
            this.uiLabel1.Text = "姓名：";
            this.uiLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel2
            // 
            this.uiLabel2.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel2.Location = new System.Drawing.Point(55, 37);
            this.uiLabel2.Name = "uiLabel2";
            this.uiLabel2.Size = new System.Drawing.Size(100, 23);
            this.uiLabel2.Style = Sunny.UI.UIStyle.Red;
            this.uiLabel2.TabIndex = 2;
            this.uiLabel2.Text = "姓名：";
            this.uiLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel3
            // 
            this.uiLabel3.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel3.Location = new System.Drawing.Point(55, 98);
            this.uiLabel3.Name = "uiLabel3";
            this.uiLabel3.Size = new System.Drawing.Size(100, 23);
            this.uiLabel3.Style = Sunny.UI.UIStyle.Red;
            this.uiLabel3.TabIndex = 2;
            this.uiLabel3.Text = "姓别：";
            this.uiLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.rbtnWomen);
            this.panel1.Controls.Add(this.rbtnMan);
            this.panel1.Location = new System.Drawing.Point(160, 89);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(217, 47);
            this.panel1.TabIndex = 3;
            // 
            // rbtnWomen
            // 
            this.rbtnWomen.Cursor = System.Windows.Forms.Cursors.Hand;
            this.rbtnWomen.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.rbtnWomen.Location = new System.Drawing.Point(133, 9);
            this.rbtnWomen.Name = "rbtnWomen";
            this.rbtnWomen.Padding = new System.Windows.Forms.Padding(22, 0, 0, 0);
            this.rbtnWomen.RadioButtonColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.rbtnWomen.Size = new System.Drawing.Size(53, 29);
            this.rbtnWomen.Style = Sunny.UI.UIStyle.Red;
            this.rbtnWomen.TabIndex = 0;
            this.rbtnWomen.Tag = "1";
            this.rbtnWomen.Text = "女";
            // 
            // rbtnMan
            // 
            this.rbtnMan.Cursor = System.Windows.Forms.Cursors.Hand;
            this.rbtnMan.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.rbtnMan.Location = new System.Drawing.Point(3, 9);
            this.rbtnMan.Name = "rbtnMan";
            this.rbtnMan.Padding = new System.Windows.Forms.Padding(22, 0, 0, 0);
            this.rbtnMan.RadioButtonColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.rbtnMan.Size = new System.Drawing.Size(53, 29);
            this.rbtnMan.Style = Sunny.UI.UIStyle.Red;
            this.rbtnMan.TabIndex = 0;
            this.rbtnMan.Tag = "0";
            this.rbtnMan.Text = "男";
            // 
            // uiLabel4
            // 
            this.uiLabel4.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel4.Location = new System.Drawing.Point(17, 150);
            this.uiLabel4.Name = "uiLabel4";
            this.uiLabel4.Size = new System.Drawing.Size(119, 23);
            this.uiLabel4.Style = Sunny.UI.UIStyle.Red;
            this.uiLabel4.TabIndex = 2;
            this.uiLabel4.Text = "歌手类型：";
            this.uiLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // comBoxSingerType
            // 
            this.comBoxSingerType.FillColor = System.Drawing.Color.White;
            this.comBoxSingerType.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.comBoxSingerType.Location = new System.Drawing.Point(162, 153);
            this.comBoxSingerType.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.comBoxSingerType.MinimumSize = new System.Drawing.Size(63, 0);
            this.comBoxSingerType.Name = "comBoxSingerType";
            this.comBoxSingerType.Padding = new System.Windows.Forms.Padding(0, 0, 30, 0);
            this.comBoxSingerType.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.comBoxSingerType.Size = new System.Drawing.Size(215, 34);
            this.comBoxSingerType.Style = Sunny.UI.UIStyle.Red;
            this.comBoxSingerType.TabIndex = 4;
            this.comBoxSingerType.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel5
            // 
            this.uiLabel5.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel5.Location = new System.Drawing.Point(14, 261);
            this.uiLabel5.Name = "uiLabel5";
            this.uiLabel5.Size = new System.Drawing.Size(119, 23);
            this.uiLabel5.Style = Sunny.UI.UIStyle.Red;
            this.uiLabel5.TabIndex = 2;
            this.uiLabel5.Text = "歌手描述：";
            this.uiLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtBoxSingerDescription
            // 
            this.txtBoxSingerDescription.AutoWordSelection = true;
            this.txtBoxSingerDescription.FillColor = System.Drawing.Color.White;
            this.txtBoxSingerDescription.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.txtBoxSingerDescription.Location = new System.Drawing.Point(160, 261);
            this.txtBoxSingerDescription.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtBoxSingerDescription.Name = "txtBoxSingerDescription";
            this.txtBoxSingerDescription.Padding = new System.Windows.Forms.Padding(2);
            this.txtBoxSingerDescription.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.txtBoxSingerDescription.Size = new System.Drawing.Size(663, 279);
            this.txtBoxSingerDescription.Style = Sunny.UI.UIStyle.Red;
            this.txtBoxSingerDescription.TabIndex = 5;
            // 
            // picBox
            // 
            this.picBox.BackColor = System.Drawing.Color.White;
            this.picBox.Location = new System.Drawing.Point(615, 37);
            this.picBox.Name = "picBox";
            this.picBox.Size = new System.Drawing.Size(145, 158);
            this.picBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picBox.TabIndex = 6;
            this.picBox.TabStop = false;
            // 
            // btnImg
            // 
            this.btnImg.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnImg.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.btnImg.FillHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(127)))), ((int)(((byte)(128)))));
            this.btnImg.FillPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(87)))), ((int)(((byte)(89)))));
            this.btnImg.FillSelectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(87)))), ((int)(((byte)(89)))));
            this.btnImg.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btnImg.Location = new System.Drawing.Point(634, 218);
            this.btnImg.Name = "btnImg";
            this.btnImg.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.btnImg.RectHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(127)))), ((int)(((byte)(128)))));
            this.btnImg.RectPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(87)))), ((int)(((byte)(89)))));
            this.btnImg.RectSelectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(87)))), ((int)(((byte)(89)))));
            this.btnImg.Size = new System.Drawing.Size(107, 35);
            this.btnImg.Style = Sunny.UI.UIStyle.Red;
            this.btnImg.TabIndex = 7;
            this.btnImg.Text = "浏览。。。";
            this.btnImg.Click += new System.EventHandler(this.btnImg_Click);
            // 
            // btnSave
            // 
            this.btnSave.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSave.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.btnSave.FillHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(127)))), ((int)(((byte)(128)))));
            this.btnSave.FillPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(87)))), ((int)(((byte)(89)))));
            this.btnSave.FillSelectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(87)))), ((int)(((byte)(89)))));
            this.btnSave.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btnSave.Location = new System.Drawing.Point(261, 548);
            this.btnSave.Name = "btnSave";
            this.btnSave.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.btnSave.RectHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(127)))), ((int)(((byte)(128)))));
            this.btnSave.RectPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(87)))), ((int)(((byte)(89)))));
            this.btnSave.RectSelectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(87)))), ((int)(((byte)(89)))));
            this.btnSave.Size = new System.Drawing.Size(107, 35);
            this.btnSave.Style = Sunny.UI.UIStyle.Red;
            this.btnSave.TabIndex = 7;
            this.btnSave.Text = "保存";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCancel.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.btnCancel.FillHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(127)))), ((int)(((byte)(128)))));
            this.btnCancel.FillPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(87)))), ((int)(((byte)(89)))));
            this.btnCancel.FillSelectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(87)))), ((int)(((byte)(89)))));
            this.btnCancel.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btnCancel.Location = new System.Drawing.Point(528, 548);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.btnCancel.RectHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(127)))), ((int)(((byte)(128)))));
            this.btnCancel.RectPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(87)))), ((int)(((byte)(89)))));
            this.btnCancel.RectSelectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(87)))), ((int)(((byte)(89)))));
            this.btnCancel.Size = new System.Drawing.Size(107, 35);
            this.btnCancel.Style = Sunny.UI.UIStyle.Red;
            this.btnCancel.TabIndex = 7;
            this.btnCancel.Text = "取消";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // ofd
            // 
            this.ofd.FileName = "openFileDialog1";
            // 
            // dsCombox
            // 
            this.dsCombox.DataSetName = "NewDataSet";
            // 
            // FrmAddSingerInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 27F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(857, 585);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnImg);
            this.Controls.Add(this.picBox);
            this.Controls.Add(this.txtBoxSingerDescription);
            this.Controls.Add(this.comBoxSingerType);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.uiLabel5);
            this.Controls.Add(this.uiLabel4);
            this.Controls.Add(this.uiLabel3);
            this.Controls.Add(this.uiLabel2);
            this.Controls.Add(this.uiLabel1);
            this.Controls.Add(this.txtBoxSingerName);
            this.Name = "FrmAddSingerInfo";
            this.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.Style = Sunny.UI.UIStyle.Red;
            this.Text = "FrmAddSingerInfo";
            this.Initialize += new System.EventHandler(this.FrmAddSingerInfo_Initialize);
            this.Load += new System.EventHandler(this.FrmAddSingerInfo_Load);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsCombox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Sunny.UI.UITextBox txtBoxSingerName;
        private Sunny.UI.UILabel uiLabel1;
        private Sunny.UI.UILabel uiLabel2;
        private Sunny.UI.UILabel uiLabel3;
        private System.Windows.Forms.Panel panel1;
        private Sunny.UI.UIRadioButton rbtnWomen;
        private Sunny.UI.UIRadioButton rbtnMan;
        private Sunny.UI.UILabel uiLabel4;
        private Sunny.UI.UIComboBox comBoxSingerType;
        private Sunny.UI.UILabel uiLabel5;
        private Sunny.UI.UIRichTextBox txtBoxSingerDescription;
        private System.Windows.Forms.PictureBox picBox;
        private Sunny.UI.UIButton btnImg;
        private Sunny.UI.UIButton btnSave;
        private Sunny.UI.UIButton btnCancel;
        private System.Windows.Forms.OpenFileDialog ofd;
        private System.Data.DataSet dsCombox;
    }
}