﻿namespace client.Pages
{
    partial class FrmAddSongs
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.uiLabel1 = new Sunny.UI.UILabel();
            this.uiLabel2 = new Sunny.UI.UILabel();
            this.uiLabel3 = new Sunny.UI.UILabel();
            this.uiLabel4 = new Sunny.UI.UILabel();
            this.uiLabel5 = new Sunny.UI.UILabel();
            this.uiLabel6 = new Sunny.UI.UILabel();
            this.txtBoxSongsName = new Sunny.UI.UITextBox();
            this.txtBoxSongsPinYin = new Sunny.UI.UITextBox();
            this.txtBoxSongsLength = new Sunny.UI.UITextBox();
            this.txtBoxFileName = new Sunny.UI.UITextBox();
            this.btnCancel = new Sunny.UI.UIButton();
            this.button1 = new Sunny.UI.UIButton();
            this.comboxSongsType = new Sunny.UI.UIComboBox();
            this.comboxSinger = new Sunny.UI.UIComboBox();
            this.btnSelect = new Sunny.UI.UIButton();
            this.ofd = new System.Windows.Forms.OpenFileDialog();
            this.dsType = new System.Data.DataSet();
            this.dsSinger = new System.Data.DataSet();
            ((System.ComponentModel.ISupportInitialize)(this.dsType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsSinger)).BeginInit();
            this.SuspendLayout();
            // 
            // uiLabel1
            // 
            this.uiLabel1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel1.Location = new System.Drawing.Point(138, 47);
            this.uiLabel1.Name = "uiLabel1";
            this.uiLabel1.Size = new System.Drawing.Size(126, 23);
            this.uiLabel1.Style = Sunny.UI.UIStyle.Red;
            this.uiLabel1.TabIndex = 0;
            this.uiLabel1.Text = "歌曲名称：";
            this.uiLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel2
            // 
            this.uiLabel2.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel2.Location = new System.Drawing.Point(138, 110);
            this.uiLabel2.Name = "uiLabel2";
            this.uiLabel2.Size = new System.Drawing.Size(126, 23);
            this.uiLabel2.Style = Sunny.UI.UIStyle.Red;
            this.uiLabel2.TabIndex = 0;
            this.uiLabel2.Text = "拼音缩写：";
            this.uiLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel3
            // 
            this.uiLabel3.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel3.Location = new System.Drawing.Point(138, 173);
            this.uiLabel3.Name = "uiLabel3";
            this.uiLabel3.Size = new System.Drawing.Size(126, 23);
            this.uiLabel3.Style = Sunny.UI.UIStyle.Red;
            this.uiLabel3.TabIndex = 0;
            this.uiLabel3.Text = "歌曲字数：";
            this.uiLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel4
            // 
            this.uiLabel4.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel4.Location = new System.Drawing.Point(138, 232);
            this.uiLabel4.Name = "uiLabel4";
            this.uiLabel4.Size = new System.Drawing.Size(126, 23);
            this.uiLabel4.Style = Sunny.UI.UIStyle.Red;
            this.uiLabel4.TabIndex = 0;
            this.uiLabel4.Text = "歌曲类型：";
            this.uiLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel5
            // 
            this.uiLabel5.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel5.Location = new System.Drawing.Point(138, 289);
            this.uiLabel5.Name = "uiLabel5";
            this.uiLabel5.Size = new System.Drawing.Size(126, 23);
            this.uiLabel5.Style = Sunny.UI.UIStyle.Red;
            this.uiLabel5.TabIndex = 0;
            this.uiLabel5.Text = "演唱歌手：";
            this.uiLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel6
            // 
            this.uiLabel6.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel6.Location = new System.Drawing.Point(136, 348);
            this.uiLabel6.Name = "uiLabel6";
            this.uiLabel6.Size = new System.Drawing.Size(126, 23);
            this.uiLabel6.Style = Sunny.UI.UIStyle.Red;
            this.uiLabel6.TabIndex = 0;
            this.uiLabel6.Text = "文件目录：";
            this.uiLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtBoxSongsName
            // 
            this.txtBoxSongsName.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtBoxSongsName.FillColor = System.Drawing.Color.White;
            this.txtBoxSongsName.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.txtBoxSongsName.Location = new System.Drawing.Point(271, 47);
            this.txtBoxSongsName.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtBoxSongsName.Maximum = 2147483647D;
            this.txtBoxSongsName.Minimum = -2147483648D;
            this.txtBoxSongsName.Name = "txtBoxSongsName";
            this.txtBoxSongsName.Padding = new System.Windows.Forms.Padding(5);
            this.txtBoxSongsName.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.txtBoxSongsName.Size = new System.Drawing.Size(340, 34);
            this.txtBoxSongsName.Style = Sunny.UI.UIStyle.Red;
            this.txtBoxSongsName.TabIndex = 1;
            // 
            // txtBoxSongsPinYin
            // 
            this.txtBoxSongsPinYin.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtBoxSongsPinYin.FillColor = System.Drawing.Color.White;
            this.txtBoxSongsPinYin.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.txtBoxSongsPinYin.Location = new System.Drawing.Point(271, 110);
            this.txtBoxSongsPinYin.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtBoxSongsPinYin.Maximum = 2147483647D;
            this.txtBoxSongsPinYin.Minimum = -2147483648D;
            this.txtBoxSongsPinYin.Name = "txtBoxSongsPinYin";
            this.txtBoxSongsPinYin.Padding = new System.Windows.Forms.Padding(5);
            this.txtBoxSongsPinYin.ReadOnly = true;
            this.txtBoxSongsPinYin.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.txtBoxSongsPinYin.Size = new System.Drawing.Size(340, 34);
            this.txtBoxSongsPinYin.Style = Sunny.UI.UIStyle.Red;
            this.txtBoxSongsPinYin.TabIndex = 2;
            // 
            // txtBoxSongsLength
            // 
            this.txtBoxSongsLength.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtBoxSongsLength.FillColor = System.Drawing.Color.White;
            this.txtBoxSongsLength.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.txtBoxSongsLength.Location = new System.Drawing.Point(271, 173);
            this.txtBoxSongsLength.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtBoxSongsLength.Maximum = 2147483647D;
            this.txtBoxSongsLength.Minimum = -2147483648D;
            this.txtBoxSongsLength.Name = "txtBoxSongsLength";
            this.txtBoxSongsLength.Padding = new System.Windows.Forms.Padding(5);
            this.txtBoxSongsLength.ReadOnly = true;
            this.txtBoxSongsLength.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.txtBoxSongsLength.Size = new System.Drawing.Size(341, 34);
            this.txtBoxSongsLength.Style = Sunny.UI.UIStyle.Red;
            this.txtBoxSongsLength.TabIndex = 3;
            // 
            // txtBoxFileName
            // 
            this.txtBoxFileName.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtBoxFileName.FillColor = System.Drawing.Color.White;
            this.txtBoxFileName.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.txtBoxFileName.Location = new System.Drawing.Point(271, 348);
            this.txtBoxFileName.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtBoxFileName.Maximum = 2147483647D;
            this.txtBoxFileName.Minimum = -2147483648D;
            this.txtBoxFileName.Name = "txtBoxFileName";
            this.txtBoxFileName.Padding = new System.Windows.Forms.Padding(5);
            this.txtBoxFileName.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.txtBoxFileName.Size = new System.Drawing.Size(341, 34);
            this.txtBoxFileName.Style = Sunny.UI.UIStyle.Red;
            this.txtBoxFileName.TabIndex = 3;
            // 
            // btnCancel
            // 
            this.btnCancel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCancel.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.btnCancel.FillHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(127)))), ((int)(((byte)(128)))));
            this.btnCancel.FillPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(87)))), ((int)(((byte)(89)))));
            this.btnCancel.FillSelectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(87)))), ((int)(((byte)(89)))));
            this.btnCancel.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btnCancel.Location = new System.Drawing.Point(453, 439);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.btnCancel.RectHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(127)))), ((int)(((byte)(128)))));
            this.btnCancel.RectPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(87)))), ((int)(((byte)(89)))));
            this.btnCancel.RectSelectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(87)))), ((int)(((byte)(89)))));
            this.btnCancel.Size = new System.Drawing.Size(157, 52);
            this.btnCancel.Style = Sunny.UI.UIStyle.Red;
            this.btnCancel.TabIndex = 4;
            this.btnCancel.Text = "取消";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // button1
            // 
            this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button1.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.button1.FillHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(127)))), ((int)(((byte)(128)))));
            this.button1.FillPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(87)))), ((int)(((byte)(89)))));
            this.button1.FillSelectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(87)))), ((int)(((byte)(89)))));
            this.button1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.button1.Location = new System.Drawing.Point(141, 439);
            this.button1.Name = "button1";
            this.button1.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.button1.RectHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(127)))), ((int)(((byte)(128)))));
            this.button1.RectPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(87)))), ((int)(((byte)(89)))));
            this.button1.RectSelectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(87)))), ((int)(((byte)(89)))));
            this.button1.Size = new System.Drawing.Size(157, 52);
            this.button1.Style = Sunny.UI.UIStyle.Red;
            this.button1.TabIndex = 4;
            this.button1.Text = "提交";
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // comboxSongsType
            // 
            this.comboxSongsType.FillColor = System.Drawing.Color.White;
            this.comboxSongsType.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.comboxSongsType.Location = new System.Drawing.Point(271, 233);
            this.comboxSongsType.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.comboxSongsType.MinimumSize = new System.Drawing.Size(63, 0);
            this.comboxSongsType.Name = "comboxSongsType";
            this.comboxSongsType.Padding = new System.Windows.Forms.Padding(0, 0, 30, 0);
            this.comboxSongsType.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.comboxSongsType.Size = new System.Drawing.Size(340, 34);
            this.comboxSongsType.Style = Sunny.UI.UIStyle.Red;
            this.comboxSongsType.TabIndex = 5;
            this.comboxSongsType.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.comboxSongsType.SelectedIndexChanged += new System.EventHandler(this.comboxSongsType_SelectedIndexChanged);
            // 
            // comboxSinger
            // 
            this.comboxSinger.FillColor = System.Drawing.Color.White;
            this.comboxSinger.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.comboxSinger.Location = new System.Drawing.Point(271, 289);
            this.comboxSinger.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.comboxSinger.MinimumSize = new System.Drawing.Size(63, 0);
            this.comboxSinger.Name = "comboxSinger";
            this.comboxSinger.Padding = new System.Windows.Forms.Padding(0, 0, 30, 0);
            this.comboxSinger.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.comboxSinger.Size = new System.Drawing.Size(339, 34);
            this.comboxSinger.Style = Sunny.UI.UIStyle.Red;
            this.comboxSinger.TabIndex = 6;
            this.comboxSinger.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnSelect
            // 
            this.btnSelect.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSelect.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.btnSelect.FillHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(127)))), ((int)(((byte)(128)))));
            this.btnSelect.FillPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(87)))), ((int)(((byte)(89)))));
            this.btnSelect.FillSelectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(87)))), ((int)(((byte)(89)))));
            this.btnSelect.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btnSelect.Location = new System.Drawing.Point(634, 348);
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.btnSelect.RectHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(127)))), ((int)(((byte)(128)))));
            this.btnSelect.RectPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(87)))), ((int)(((byte)(89)))));
            this.btnSelect.RectSelectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(87)))), ((int)(((byte)(89)))));
            this.btnSelect.Size = new System.Drawing.Size(94, 34);
            this.btnSelect.Style = Sunny.UI.UIStyle.Red;
            this.btnSelect.TabIndex = 4;
            this.btnSelect.Text = "浏览。。";
            this.btnSelect.Click += new System.EventHandler(this.btnSelect_Click);
            // 
            // ofd
            // 
            this.ofd.FileName = "openFileDialog1";
            // 
            // dsType
            // 
            this.dsType.DataSetName = "NewDataSet";
            // 
            // dsSinger
            // 
            this.dsSinger.DataSetName = "NewDataSet";
            // 
            // FrmAddSongs
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 27F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(883, 558);
            this.Controls.Add(this.comboxSinger);
            this.Controls.Add(this.comboxSongsType);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnSelect);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.txtBoxFileName);
            this.Controls.Add(this.txtBoxSongsLength);
            this.Controls.Add(this.txtBoxSongsPinYin);
            this.Controls.Add(this.txtBoxSongsName);
            this.Controls.Add(this.uiLabel6);
            this.Controls.Add(this.uiLabel5);
            this.Controls.Add(this.uiLabel4);
            this.Controls.Add(this.uiLabel3);
            this.Controls.Add(this.uiLabel2);
            this.Controls.Add(this.uiLabel1);
            this.Name = "FrmAddSongs";
            this.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.Style = Sunny.UI.UIStyle.Red;
            this.Text = "FrmAddSongs";
            this.Load += new System.EventHandler(this.FrmAddSongs_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dsType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsSinger)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Sunny.UI.UILabel uiLabel1;
        private Sunny.UI.UILabel uiLabel2;
        private Sunny.UI.UILabel uiLabel3;
        private Sunny.UI.UILabel uiLabel4;
        private Sunny.UI.UILabel uiLabel5;
        private Sunny.UI.UILabel uiLabel6;
        private Sunny.UI.UITextBox txtBoxSongsName;
        private Sunny.UI.UITextBox txtBoxSongsPinYin;
        private Sunny.UI.UITextBox txtBoxSongsLength;
        private Sunny.UI.UITextBox txtBoxFileName;
        private Sunny.UI.UIButton btnCancel;
        private Sunny.UI.UIButton button1;
        private Sunny.UI.UIComboBox comboxSongsType;
        private Sunny.UI.UIComboBox comboxSinger;
        private Sunny.UI.UIButton btnSelect;
        private System.Windows.Forms.OpenFileDialog ofd;
        private System.Data.DataSet dsType;
        private System.Data.DataSet dsSinger;
    }
}