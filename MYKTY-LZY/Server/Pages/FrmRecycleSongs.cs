﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Sunny.UI;

namespace client.Pages
{
    public partial class FrmRecycleSongs : UIPage
    {
        public FrmRecycleSongs()
        {
            InitializeComponent();
        }

        DBHandle dBHandle = new DBHandle();
        SingerInfo singerInfo = new SingerInfo();
        public void SelectSongsInfo()//查询歌手信息
        {

            if (this.txtBoxName.Text.Trim().Equals("请输入歌曲名") || this.txtBoxName.Text.Trim().Equals(string.Empty))
            {
                SqlDataAdapter adapter = dBHandle.SelectInfoBySQl(8);
                dsDgv = new DataSet();
                adapter.Fill(dsDgv, "info");
                this.dataGridView1.DataSource = dsDgv.Tables["info"];
            }
            else
            {
                singerInfo.Name = (this.txtBoxName.Text).ToUpper();

                SqlDataAdapter adapter2 = dBHandle.SelectInfoBySQl(10, singerInfo);

                DataSet dsDgv1 = new DataSet();

                adapter2.Fill(dsDgv1, "info");

                this.dataGridView1.DataSource = dsDgv1.Tables["info"];
            }
        }

        public void RecycleSongs()
        {
            int id = Convert.ToInt32(this.dataGridView1.SelectedCells[7].Value);
            bool result = dBHandle.RecycleSongs(1, id);
            if (result == true)
            {
                this.ShowInfoDialog("恢复成功！", UIStyle.Red);
                SelectSongsInfo();
            }
            else
            {
                this.ShowInfoDialog("恢复失败！", UIStyle.Red);
            }

        }

        private void FrmRecycleSongs_Load(object sender, EventArgs e)
        {
            dBHandle.DeleteById(1);
            this.dataGridView1.AutoGenerateColumns = false;
            SelectSongsInfo();
        }

        private void uiButton1_Click(object sender, EventArgs e)
        {
            SelectSongsInfo();
        }

        private void 恢复ToolStripMenuItem1_Click(object sender, EventArgs e)
        {           
            try
            {
                if (this.dataGridView1.SelectedRows.Count > 0)
                {
                    RecycleSongs();
                }
                else
                {
                    this.ShowInfoDialog("请选中一行！", UIStyle.Red);
                }
            }
            catch (Exception)
            {
                this.ShowInfoDialog("请选中一行！", UIStyle.Red);
            }
        }

        private void txtBoxName_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
