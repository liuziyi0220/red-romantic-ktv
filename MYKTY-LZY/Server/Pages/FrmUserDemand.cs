﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Sunny.UI;

namespace client.Pages
{
    public partial class FrmUserDemand : UIPage
    {
        DBHandle dBHandle = new DBHandle();
        public FrmUserDemand()
        {            
            InitializeComponent();
            GetInfo(10);         
        }
        SqlDataAdapter sqlDataAdapte=null;
        public void GetInfo(int choes)
        {
            sqlDataAdapte = dBHandle.SelectInfoBySQl(choes);
            dsDgv = new DataSet();
            sqlDataAdapte.Fill(this.dsDgv,"info");            
            this.uiDataGridView1.DataSource = dsDgv.Tables["info"];
        }

        private void btnIsRead_Click(object sender, EventArgs e)
        {
            if (Convert.ToInt32(btnIsRead.Tag)==0)
            {
                btnIsRead.Tag = 1;
                btnIsRead.Text = "查看未读";
                GetInfo(11);
               
            }
            else
            {
                btnIsRead.Tag = 0;
                btnIsRead.Text = "查看已读";      
                GetInfo(10);
            }
        }

        public void ChangeIsRead()
        {           
            bool result=dBHandle.IsRead(Convert.ToInt32(uiDataGridView1.SelectedCells[0].Value));
            if (!result)
            {
                this.ShowErrorDialog("错误!");
            }
        }
        private void FrmUserDemand_Load(object sender, EventArgs e)
        {
            this.uiDataGridView1.AutoGenerateColumns = false;
        }

        private void 已读ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                GetInfo(10);
                ChangeIsRead();
            }
            catch (Exception)
            {
                this.ShowErrorDialog("请选中一行!");
            }
        }
        int result =0;
        int oldresult = 0;
        private void timer_Tick(object sender, EventArgs e)
        {         
            if (result!=oldresult)
            {
                oldresult = result;
                GetInfo(10);
                this.ShowInfoNotifier("您有新消息！");
            }
            this.result = dBHandle.HaveNewMessqge();
        }
    }
}
