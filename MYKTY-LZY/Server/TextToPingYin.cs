﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.International.Converters.PinYinConverter;


namespace client
{
    class TextToPingYin
    {
        public string GetPinyin(string str)

        {

            string r = string.Empty;

            foreach (char obj in str)

            {

                try

                {
                    ChineseChar chineseChar = new ChineseChar(obj);

                    string t = chineseChar.Pinyins[0].ToString();

                    r += t.Substring(0, t.Length - 1);
                }
                catch

                {
                    r += obj.ToString();
                }
            }
            return r;
        }
    }
}
