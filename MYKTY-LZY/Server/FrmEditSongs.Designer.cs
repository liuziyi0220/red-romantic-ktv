﻿namespace client
{
    partial class FrmEditSongs
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.uiLabel1 = new Sunny.UI.UILabel();
            this.uiLabel2 = new Sunny.UI.UILabel();
            this.uiLabel3 = new Sunny.UI.UILabel();
            this.uiLabel4 = new Sunny.UI.UILabel();
            this.uiLabel5 = new Sunny.UI.UILabel();
            this.uiLabel6 = new Sunny.UI.UILabel();
            this.txtBoxSongsName = new Sunny.UI.UITextBox();
            this.txtBoxSongsPinYin = new Sunny.UI.UITextBox();
            this.txtBoxFileName = new Sunny.UI.UITextBox();
            this.txtBoxSongsLength = new Sunny.UI.UITextBox();
            this.comboxSongsType = new Sunny.UI.UIComboBox();
            this.comboxSinger = new Sunny.UI.UIComboBox();
            this.btnSelect = new Sunny.UI.UIButton();
            this.btnSubmit = new Sunny.UI.UIButton();
            this.btnCancel = new Sunny.UI.UIButton();
            this.btnDelete = new Sunny.UI.UIButton();
            this.ofd = new System.Windows.Forms.OpenFileDialog();
            this.SuspendLayout();
            // 
            // uiLabel1
            // 
            this.uiLabel1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel1.Location = new System.Drawing.Point(113, 81);
            this.uiLabel1.Name = "uiLabel1";
            this.uiLabel1.Size = new System.Drawing.Size(112, 23);
            this.uiLabel1.Style = Sunny.UI.UIStyle.Custom;
            this.uiLabel1.TabIndex = 0;
            this.uiLabel1.Text = "歌曲名称：";
            this.uiLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel2
            // 
            this.uiLabel2.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel2.Location = new System.Drawing.Point(113, 135);
            this.uiLabel2.Name = "uiLabel2";
            this.uiLabel2.Size = new System.Drawing.Size(112, 23);
            this.uiLabel2.Style = Sunny.UI.UIStyle.Custom;
            this.uiLabel2.TabIndex = 1;
            this.uiLabel2.Text = "歌名拼音：";
            this.uiLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel3
            // 
            this.uiLabel3.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel3.Location = new System.Drawing.Point(113, 186);
            this.uiLabel3.Name = "uiLabel3";
            this.uiLabel3.Size = new System.Drawing.Size(112, 23);
            this.uiLabel3.Style = Sunny.UI.UIStyle.Custom;
            this.uiLabel3.TabIndex = 1;
            this.uiLabel3.Text = "歌名长度：";
            this.uiLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel4
            // 
            this.uiLabel4.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel4.Location = new System.Drawing.Point(113, 239);
            this.uiLabel4.Name = "uiLabel4";
            this.uiLabel4.Size = new System.Drawing.Size(112, 23);
            this.uiLabel4.Style = Sunny.UI.UIStyle.Custom;
            this.uiLabel4.TabIndex = 1;
            this.uiLabel4.Text = "歌曲类型：";
            this.uiLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel5
            // 
            this.uiLabel5.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel5.Location = new System.Drawing.Point(113, 291);
            this.uiLabel5.Name = "uiLabel5";
            this.uiLabel5.Size = new System.Drawing.Size(112, 23);
            this.uiLabel5.Style = Sunny.UI.UIStyle.Custom;
            this.uiLabel5.TabIndex = 1;
            this.uiLabel5.Text = "演唱歌手：";
            this.uiLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel6
            // 
            this.uiLabel6.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel6.Location = new System.Drawing.Point(113, 343);
            this.uiLabel6.Name = "uiLabel6";
            this.uiLabel6.Size = new System.Drawing.Size(112, 23);
            this.uiLabel6.Style = Sunny.UI.UIStyle.Custom;
            this.uiLabel6.TabIndex = 1;
            this.uiLabel6.Text = "文件目录：";
            this.uiLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtBoxSongsName
            // 
            this.txtBoxSongsName.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtBoxSongsName.FillColor = System.Drawing.Color.White;
            this.txtBoxSongsName.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.txtBoxSongsName.Location = new System.Drawing.Point(241, 81);
            this.txtBoxSongsName.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtBoxSongsName.Maximum = 2147483647D;
            this.txtBoxSongsName.Minimum = -2147483648D;
            this.txtBoxSongsName.Name = "txtBoxSongsName";
            this.txtBoxSongsName.Padding = new System.Windows.Forms.Padding(5);
            this.txtBoxSongsName.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.txtBoxSongsName.Size = new System.Drawing.Size(234, 34);
            this.txtBoxSongsName.Style = Sunny.UI.UIStyle.Red;
            this.txtBoxSongsName.TabIndex = 2;
            // 
            // txtBoxSongsPinYin
            // 
            this.txtBoxSongsPinYin.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtBoxSongsPinYin.FillColor = System.Drawing.Color.White;
            this.txtBoxSongsPinYin.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.txtBoxSongsPinYin.Location = new System.Drawing.Point(241, 135);
            this.txtBoxSongsPinYin.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtBoxSongsPinYin.Maximum = 2147483647D;
            this.txtBoxSongsPinYin.Minimum = -2147483648D;
            this.txtBoxSongsPinYin.Name = "txtBoxSongsPinYin";
            this.txtBoxSongsPinYin.Padding = new System.Windows.Forms.Padding(5);
            this.txtBoxSongsPinYin.ReadOnly = true;
            this.txtBoxSongsPinYin.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.txtBoxSongsPinYin.Size = new System.Drawing.Size(234, 34);
            this.txtBoxSongsPinYin.Style = Sunny.UI.UIStyle.Custom;
            this.txtBoxSongsPinYin.TabIndex = 2;
            // 
            // txtBoxFileName
            // 
            this.txtBoxFileName.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtBoxFileName.FillColor = System.Drawing.Color.White;
            this.txtBoxFileName.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.txtBoxFileName.Location = new System.Drawing.Point(241, 343);
            this.txtBoxFileName.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtBoxFileName.Maximum = 2147483647D;
            this.txtBoxFileName.Minimum = -2147483648D;
            this.txtBoxFileName.Name = "txtBoxFileName";
            this.txtBoxFileName.Padding = new System.Windows.Forms.Padding(5);
            this.txtBoxFileName.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.txtBoxFileName.Size = new System.Drawing.Size(234, 34);
            this.txtBoxFileName.Style = Sunny.UI.UIStyle.Custom;
            this.txtBoxFileName.TabIndex = 2;
            // 
            // txtBoxSongsLength
            // 
            this.txtBoxSongsLength.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtBoxSongsLength.FillColor = System.Drawing.Color.White;
            this.txtBoxSongsLength.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.txtBoxSongsLength.Location = new System.Drawing.Point(241, 186);
            this.txtBoxSongsLength.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtBoxSongsLength.Maximum = 2147483647D;
            this.txtBoxSongsLength.Minimum = -2147483648D;
            this.txtBoxSongsLength.Name = "txtBoxSongsLength";
            this.txtBoxSongsLength.Padding = new System.Windows.Forms.Padding(5);
            this.txtBoxSongsLength.ReadOnly = true;
            this.txtBoxSongsLength.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.txtBoxSongsLength.Size = new System.Drawing.Size(234, 34);
            this.txtBoxSongsLength.Style = Sunny.UI.UIStyle.Custom;
            this.txtBoxSongsLength.TabIndex = 2;
            // 
            // comboxSongsType
            // 
            this.comboxSongsType.FillColor = System.Drawing.Color.White;
            this.comboxSongsType.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.comboxSongsType.Location = new System.Drawing.Point(241, 239);
            this.comboxSongsType.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.comboxSongsType.MinimumSize = new System.Drawing.Size(63, 0);
            this.comboxSongsType.Name = "comboxSongsType";
            this.comboxSongsType.Padding = new System.Windows.Forms.Padding(0, 0, 30, 0);
            this.comboxSongsType.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.comboxSongsType.Size = new System.Drawing.Size(234, 34);
            this.comboxSongsType.Style = Sunny.UI.UIStyle.Red;
            this.comboxSongsType.TabIndex = 3;
            this.comboxSongsType.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // comboxSinger
            // 
            this.comboxSinger.FillColor = System.Drawing.Color.White;
            this.comboxSinger.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.comboxSinger.Location = new System.Drawing.Point(241, 291);
            this.comboxSinger.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.comboxSinger.MinimumSize = new System.Drawing.Size(63, 0);
            this.comboxSinger.Name = "comboxSinger";
            this.comboxSinger.Padding = new System.Windows.Forms.Padding(0, 0, 30, 0);
            this.comboxSinger.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.comboxSinger.Size = new System.Drawing.Size(234, 34);
            this.comboxSinger.Style = Sunny.UI.UIStyle.Red;
            this.comboxSinger.TabIndex = 3;
            this.comboxSinger.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnSelect
            // 
            this.btnSelect.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSelect.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.btnSelect.FillHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(127)))), ((int)(((byte)(128)))));
            this.btnSelect.FillPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(87)))), ((int)(((byte)(89)))));
            this.btnSelect.FillSelectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(87)))), ((int)(((byte)(89)))));
            this.btnSelect.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btnSelect.Location = new System.Drawing.Point(497, 343);
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.btnSelect.RectHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(127)))), ((int)(((byte)(128)))));
            this.btnSelect.RectPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(87)))), ((int)(((byte)(89)))));
            this.btnSelect.RectSelectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(87)))), ((int)(((byte)(89)))));
            this.btnSelect.Size = new System.Drawing.Size(100, 33);
            this.btnSelect.Style = Sunny.UI.UIStyle.Red;
            this.btnSelect.TabIndex = 4;
            this.btnSelect.Text = "浏览。。";
            this.btnSelect.Click += new System.EventHandler(this.btnSelect_Click);
            // 
            // btnSubmit
            // 
            this.btnSubmit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSubmit.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.btnSubmit.FillHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(127)))), ((int)(((byte)(128)))));
            this.btnSubmit.FillPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(87)))), ((int)(((byte)(89)))));
            this.btnSubmit.FillSelectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(87)))), ((int)(((byte)(89)))));
            this.btnSubmit.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btnSubmit.Location = new System.Drawing.Point(81, 428);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.btnSubmit.RectHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(127)))), ((int)(((byte)(128)))));
            this.btnSubmit.RectPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(87)))), ((int)(((byte)(89)))));
            this.btnSubmit.RectSelectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(87)))), ((int)(((byte)(89)))));
            this.btnSubmit.Size = new System.Drawing.Size(100, 33);
            this.btnSubmit.Style = Sunny.UI.UIStyle.Red;
            this.btnSubmit.TabIndex = 4;
            this.btnSubmit.Text = "提交";
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCancel.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.btnCancel.FillHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(127)))), ((int)(((byte)(128)))));
            this.btnCancel.FillPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(87)))), ((int)(((byte)(89)))));
            this.btnCancel.FillSelectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(87)))), ((int)(((byte)(89)))));
            this.btnCancel.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btnCancel.Location = new System.Drawing.Point(302, 428);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.btnCancel.RectHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(127)))), ((int)(((byte)(128)))));
            this.btnCancel.RectPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(87)))), ((int)(((byte)(89)))));
            this.btnCancel.RectSelectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(87)))), ((int)(((byte)(89)))));
            this.btnCancel.Size = new System.Drawing.Size(100, 33);
            this.btnCancel.Style = Sunny.UI.UIStyle.Red;
            this.btnCancel.TabIndex = 4;
            this.btnCancel.Text = "取消";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnDelete.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.btnDelete.FillHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(127)))), ((int)(((byte)(128)))));
            this.btnDelete.FillPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(87)))), ((int)(((byte)(89)))));
            this.btnDelete.FillSelectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(87)))), ((int)(((byte)(89)))));
            this.btnDelete.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btnDelete.Location = new System.Drawing.Point(497, 428);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.btnDelete.RectHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(127)))), ((int)(((byte)(128)))));
            this.btnDelete.RectPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(87)))), ((int)(((byte)(89)))));
            this.btnDelete.RectSelectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(87)))), ((int)(((byte)(89)))));
            this.btnDelete.Size = new System.Drawing.Size(140, 33);
            this.btnDelete.Style = Sunny.UI.UIStyle.Custom;
            this.btnDelete.TabIndex = 4;
            this.btnDelete.Text = "删除歌曲。。";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // ofd
            // 
            this.ofd.FileName = "openFileDialog1";
            // 
            // FrmEditSongs
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 27F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(673, 506);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSubmit);
            this.Controls.Add(this.btnSelect);
            this.Controls.Add(this.comboxSinger);
            this.Controls.Add(this.comboxSongsType);
            this.Controls.Add(this.txtBoxFileName);
            this.Controls.Add(this.txtBoxSongsLength);
            this.Controls.Add(this.txtBoxSongsPinYin);
            this.Controls.Add(this.txtBoxSongsName);
            this.Controls.Add(this.uiLabel6);
            this.Controls.Add(this.uiLabel5);
            this.Controls.Add(this.uiLabel4);
            this.Controls.Add(this.uiLabel3);
            this.Controls.Add(this.uiLabel2);
            this.Controls.Add(this.uiLabel1);
            this.Name = "FrmEditSongs";
            this.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.Style = Sunny.UI.UIStyle.Red;
            this.Text = "编辑歌曲信息";
            this.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.Load += new System.EventHandler(this.FrmEditSongs_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private Sunny.UI.UILabel uiLabel1;
        private Sunny.UI.UILabel uiLabel2;
        private Sunny.UI.UILabel uiLabel3;
        private Sunny.UI.UILabel uiLabel4;
        private Sunny.UI.UILabel uiLabel5;
        private Sunny.UI.UILabel uiLabel6;
        private Sunny.UI.UITextBox txtBoxSongsName;
        private Sunny.UI.UITextBox txtBoxSongsPinYin;
        private Sunny.UI.UITextBox txtBoxFileName;
        private Sunny.UI.UITextBox txtBoxSongsLength;
        private Sunny.UI.UIComboBox comboxSongsType;
        private Sunny.UI.UIComboBox comboxSinger;
        private Sunny.UI.UIButton btnSelect;
        private Sunny.UI.UIButton btnSubmit;
        private Sunny.UI.UIButton btnCancel;
        private Sunny.UI.UIButton btnDelete;
        private System.Windows.Forms.OpenFileDialog ofd;
    }
}