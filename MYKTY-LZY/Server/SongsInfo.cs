﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace client
{
    class SongsInfo
    {
        private int _id;
        private string _SongsName;
        private string _Spell;
        private int _NameLength;
        private int _SongsType;
        private int _Singer;
        private string _FileName;
        private int _playNum;
        private string _LastUpdateTime;
        private string _AdminName;
        private string _UpdateReason;
        private int _IsDelete;

        public string SongsName { get => _SongsName; set => _SongsName = value; }
        public string Spell { get => _Spell; set => _Spell = value; }
        public int NameLength { get => _NameLength; set => _NameLength = value; }
        public int SongsType { get => _SongsType; set => _SongsType = value; }
        public int Singer { get => _Singer; set => _Singer = value; }
        public string FileName { get => _FileName; set => _FileName = value; }
        public int PlayNum { get => _playNum; set => _playNum = value; }
        public string LastUpdateTime { get => _LastUpdateTime; set => _LastUpdateTime = value; }
        public string AdminName { get => _AdminName; set => _AdminName = value; }
        public string UpdateReason { get => _UpdateReason; set => _UpdateReason = value; }
        public int IsDelete { get => _IsDelete; set => _IsDelete = value; }
        public int Id { get => _id; set => _id = value; }
    }
}
