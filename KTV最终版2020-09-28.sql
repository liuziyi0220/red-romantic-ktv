USE [RedRomanticKTV]
GO
/****** Object:  Table [dbo].[Gender]    Script Date: 2020/9/28 13:52:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Gender](
	[id] [int] NOT NULL,
	[Gender] [nvarchar](5) NULL,
 CONSTRAINT [PK_Gender] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SingerInfo]    Script Date: 2020/9/28 13:52:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SingerInfo](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Gender] [int] NOT NULL,
	[SingerType] [int] NOT NULL,
	[Singerdescription] [nvarchar](200) NULL,
	[SingerImg] [nvarchar](200) NOT NULL,
	[LastUpdateTime] [datetime] NOT NULL,
	[AdminName] [nvarchar](50) NOT NULL,
	[UpdateReason] [nvarchar](50) NOT NULL,
	[IsDelete] [int] NULL,
 CONSTRAINT [PK_SingerInfo] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SingerType]    Script Date: 2020/9/28 13:52:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SingerType](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[TypeName] [nvarchar](50) NOT NULL,
	[LastUpdateTime] [datetime] NULL,
	[AdminName] [nvarchar](50) NULL,
	[UpdateReason] [nvarchar](50) NULL,
	[IsDelete] [int] NULL,
 CONSTRAINT [PK_SingerType] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SongsInfo]    Script Date: 2020/9/28 13:52:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SongsInfo](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[SongsName] [nvarchar](100) NOT NULL,
	[Spell] [nvarchar](50) NOT NULL,
	[NameLength] [int] NOT NULL,
	[SongsType] [int] NOT NULL,
	[Singer] [int] NOT NULL,
	[FileName] [nvarchar](100) NOT NULL,
	[playNum] [int] NOT NULL,
	[LastUpdateTime] [datetime] NOT NULL,
	[AdminName] [nvarchar](50) NOT NULL,
	[UpdateReason] [nvarchar](50) NOT NULL,
	[IsDelete] [int] NULL,
 CONSTRAINT [PK_SongsInfo] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SongsType]    Script Date: 2020/9/28 13:52:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SongsType](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[TypeName] [nvarchar](50) NOT NULL,
	[LastUpdateTime] [datetime] NOT NULL,
	[AdminName] [nvarchar](50) NOT NULL,
	[UpdateReason] [nvarchar](50) NOT NULL,
	[IsDelete] [int] NOT NULL,
 CONSTRAINT [PK_SongsType] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserInfo]    Script Date: 2020/9/28 13:52:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserInfo](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [nvarchar](50) NOT NULL,
	[PassWord] [nvarchar](500) NOT NULL,
 CONSTRAINT [PK_UserInfo] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserRequest]    Script Date: 2020/9/28 13:52:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserRequest](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[request] [nvarchar](200) NULL,
	[requestTime] [datetime] NULL,
	[isRead] [int] NULL,
 CONSTRAINT [PK_UserRequest] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Gender] ([id], [Gender]) VALUES (0, N'男')
INSERT [dbo].[Gender] ([id], [Gender]) VALUES (1, N'女')
SET IDENTITY_INSERT [dbo].[SingerInfo] ON 

INSERT [dbo].[SingerInfo] ([id], [Name], [Gender], [SingerType], [Singerdescription], [SingerImg], [LastUpdateTime], [AdminName], [UpdateReason], [IsDelete]) VALUES (2013, N'陈奕迅', 0, 1, N'', N'E:\KTVImage\陈奕迅.png', CAST(N'2020-09-04T07:15:09.607' AS DateTime), N'909090', N'通过管理员账户添加', 0)
INSERT [dbo].[SingerInfo] ([id], [Name], [Gender], [SingerType], [Singerdescription], [SingerImg], [LastUpdateTime], [AdminName], [UpdateReason], [IsDelete]) VALUES (2014, N'金沙', 1, 1, N'', N'E:\KTVImage\金莎.png', CAST(N'2020-09-04T07:17:42.350' AS DateTime), N'909090', N'通过管理员账户添加', 0)
INSERT [dbo].[SingerInfo] ([id], [Name], [Gender], [SingerType], [Singerdescription], [SingerImg], [LastUpdateTime], [AdminName], [UpdateReason], [IsDelete]) VALUES (2015, N'吴亦凡', 0, 2, N'', N'E:\KTVImage\吴亦凡.png', CAST(N'2020-09-04T07:18:04.373' AS DateTime), N'909090', N'通过管理员账户添加', 0)
INSERT [dbo].[SingerInfo] ([id], [Name], [Gender], [SingerType], [Singerdescription], [SingerImg], [LastUpdateTime], [AdminName], [UpdateReason], [IsDelete]) VALUES (2016, N'薛之谦', 0, 1, N'', N'E:\KTVImage\薛之谦.jfif', CAST(N'2020-09-04T07:19:07.340' AS DateTime), N'909090', N'通过管理员账户添加', 0)
INSERT [dbo].[SingerInfo] ([id], [Name], [Gender], [SingerType], [Singerdescription], [SingerImg], [LastUpdateTime], [AdminName], [UpdateReason], [IsDelete]) VALUES (2017, N'叶炫清', 1, 1, N'', N'E:\KTVImage\叶炫清.jfif', CAST(N'2020-09-04T07:19:20.620' AS DateTime), N'909090', N'通过管理员账户添加', 0)
INSERT [dbo].[SingerInfo] ([id], [Name], [Gender], [SingerType], [Singerdescription], [SingerImg], [LastUpdateTime], [AdminName], [UpdateReason], [IsDelete]) VALUES (2019, N'张杰', 0, 1, N'', N'E:\KTVImage\张杰.png', CAST(N'2020-09-04T07:20:45.597' AS DateTime), N'909090', N'通过管理员账户添加', 0)
INSERT [dbo].[SingerInfo] ([id], [Name], [Gender], [SingerType], [Singerdescription], [SingerImg], [LastUpdateTime], [AdminName], [UpdateReason], [IsDelete]) VALUES (2020, N'周杰伦', 0, 3, N'', N'E:\KTVImage\周杰伦3.png', CAST(N'2020-09-04T07:21:49.103' AS DateTime), N'909090', N'通过管理员账户添加', 0)
INSERT [dbo].[SingerInfo] ([id], [Name], [Gender], [SingerType], [Singerdescription], [SingerImg], [LastUpdateTime], [AdminName], [UpdateReason], [IsDelete]) VALUES (2021, N'坂田一giao', 0, 4, N'', N'E:\KTVImage\日本giao.jpg', CAST(N'2020-09-04T07:22:48.480' AS DateTime), N'909090', N'通过管理员账户添加', 0)
INSERT [dbo].[SingerInfo] ([id], [Name], [Gender], [SingerType], [Singerdescription], [SingerImg], [LastUpdateTime], [AdminName], [UpdateReason], [IsDelete]) VALUES (2022, N'张敬轩', 0, 3, N'', N'E:\KTVImage\张敬轩.jpg', CAST(N'2020-09-04T07:35:56.757' AS DateTime), N'', N'通过管理员账户添加', 0)
INSERT [dbo].[SingerInfo] ([id], [Name], [Gender], [SingerType], [Singerdescription], [SingerImg], [LastUpdateTime], [AdminName], [UpdateReason], [IsDelete]) VALUES (2023, N'林俊杰', 0, 5, N'', N'E:\KTVImage\林俊杰.jpg', CAST(N'2020-09-04T07:36:12.417' AS DateTime), N'', N'通过管理员账户添加', 0)
INSERT [dbo].[SingerInfo] ([id], [Name], [Gender], [SingerType], [Singerdescription], [SingerImg], [LastUpdateTime], [AdminName], [UpdateReason], [IsDelete]) VALUES (2024, N'林宥嘉', 0, 3, N'', N'E:\KTVImage\林宥嘉.jpg', CAST(N'2020-09-04T07:37:04.960' AS DateTime), N'', N'通过管理员账户添加', 0)
INSERT [dbo].[SingerInfo] ([id], [Name], [Gender], [SingerType], [Singerdescription], [SingerImg], [LastUpdateTime], [AdminName], [UpdateReason], [IsDelete]) VALUES (2025, N'邓紫棋', 1, 1, N'', N'E:\KTVImage\邓紫棋.png', CAST(N'2020-09-04T07:37:42.190' AS DateTime), N'', N'通过管理员账户添加', 0)
INSERT [dbo].[SingerInfo] ([id], [Name], [Gender], [SingerType], [Singerdescription], [SingerImg], [LastUpdateTime], [AdminName], [UpdateReason], [IsDelete]) VALUES (2026, N'许嵩', 0, 1, N'', N'E:\KTVImage\许嵩.jpg', CAST(N'2020-09-04T07:44:42.627' AS DateTime), N'', N'通过管理员账户添加', 0)
INSERT [dbo].[SingerInfo] ([id], [Name], [Gender], [SingerType], [Singerdescription], [SingerImg], [LastUpdateTime], [AdminName], [UpdateReason], [IsDelete]) VALUES (3014, N'deom', 0, 1, N'', N'C:\Users\MSI-CN\Pictures\Saved Pictures\UPCK{5W9JHC]SL0971MKU}P.png', CAST(N'2020-09-19T12:24:57.460' AS DateTime), N'', N'通过管理员账户添加', 0)
INSERT [dbo].[SingerInfo] ([id], [Name], [Gender], [SingerType], [Singerdescription], [SingerImg], [LastUpdateTime], [AdminName], [UpdateReason], [IsDelete]) VALUES (4014, N'demo2', 0, 1, N'ggg', N'C:\Users\MSI-CN\Pictures\Saved Pictures\1555030781.jpg', CAST(N'2020-09-19T13:35:11.130' AS DateTime), N'', N'通过管理员账户添加', 0)
INSERT [dbo].[SingerInfo] ([id], [Name], [Gender], [SingerType], [Singerdescription], [SingerImg], [LastUpdateTime], [AdminName], [UpdateReason], [IsDelete]) VALUES (4015, N'deom3', 1, 1, N'', N'C:\Users\MSI-CN\Pictures\Saved Pictures\155503078232.jpg', CAST(N'2020-09-19T13:56:27.577' AS DateTime), N'', N'通过管理员账户添加', 0)
SET IDENTITY_INSERT [dbo].[SingerInfo] OFF
SET IDENTITY_INSERT [dbo].[SingerType] ON 

INSERT [dbo].[SingerType] ([id], [TypeName], [LastUpdateTime], [AdminName], [UpdateReason], [IsDelete]) VALUES (1, N'大陆', CAST(N'2020-08-22T15:03:47.653' AS DateTime), N'909090', N'无', 0)
INSERT [dbo].[SingerType] ([id], [TypeName], [LastUpdateTime], [AdminName], [UpdateReason], [IsDelete]) VALUES (2, N'欧美', CAST(N'2020-08-22T15:04:03.207' AS DateTime), N'909090', N'无', 0)
INSERT [dbo].[SingerType] ([id], [TypeName], [LastUpdateTime], [AdminName], [UpdateReason], [IsDelete]) VALUES (3, N'港澳台', CAST(N'2020-08-22T15:04:17.253' AS DateTime), N'909090', N'无', 0)
INSERT [dbo].[SingerType] ([id], [TypeName], [LastUpdateTime], [AdminName], [UpdateReason], [IsDelete]) VALUES (4, N'日本', CAST(N'2020-08-22T15:04:25.397' AS DateTime), N'909090', N'无', 0)
INSERT [dbo].[SingerType] ([id], [TypeName], [LastUpdateTime], [AdminName], [UpdateReason], [IsDelete]) VALUES (5, N'东南亚', CAST(N'2020-08-22T15:05:22.270' AS DateTime), N'909090', N'无', 0)
SET IDENTITY_INSERT [dbo].[SingerType] OFF
SET IDENTITY_INSERT [dbo].[SongsInfo] ON 

INSERT [dbo].[SongsInfo] ([id], [SongsName], [Spell], [NameLength], [SongsType], [Singer], [FileName], [playNum], [LastUpdateTime], [AdminName], [UpdateReason], [IsDelete]) VALUES (2002, N'残酷月光', N'CANKUYUEGUANG', 4, 1, 2024, N'E:\KTVMp3\残酷月光.mp3', 0, CAST(N'2020-09-04T07:42:02.703' AS DateTime), N'909090', N'通过管理员账户添加', 0)
INSERT [dbo].[SongsInfo] ([id], [SongsName], [Spell], [NameLength], [SongsType], [Singer], [FileName], [playNum], [LastUpdateTime], [AdminName], [UpdateReason], [IsDelete]) VALUES (2003, N'大碗宽面', N'DAWANKUANMIAN', 4, 2, 2015, N'E:\KTVMp3\大碗宽面.mp3', 0, CAST(N'2020-09-04T07:42:42.537' AS DateTime), N'909090', N'通过管理员账户添加', 0)
INSERT [dbo].[SongsInfo] ([id], [SongsName], [Spell], [NameLength], [SongsType], [Singer], [FileName], [playNum], [LastUpdateTime], [AdminName], [UpdateReason], [IsDelete]) VALUES (2004, N'动物世界', N'DONGWUSHIJIE', 4, 1, 2016, N'E:\KTVMp3\动物世界.mp3', 0, CAST(N'2020-09-04T07:43:01.497' AS DateTime), N'909090', N'通过管理员账户添加', 0)
INSERT [dbo].[SongsInfo] ([id], [SongsName], [Spell], [NameLength], [SongsType], [Singer], [FileName], [playNum], [LastUpdateTime], [AdminName], [UpdateReason], [IsDelete]) VALUES (2005, N'断桥残雪', N'DUANQIAOCANXUE', 4, 1, 2026, N'E:\KTVMp3\断桥残雪.mp3', 0, CAST(N'2020-09-04T07:45:16.717' AS DateTime), N'909090', N'通过管理员账户添加', 0)
INSERT [dbo].[SongsInfo] ([id], [SongsName], [Spell], [NameLength], [SongsType], [Singer], [FileName], [playNum], [LastUpdateTime], [AdminName], [UpdateReason], [IsDelete]) VALUES (2006, N'蝴蝶的时间', N'HUDIEDESHIJIAN', 5, 1, 2026, N'E:\KTVMp3\蝴蝶的时间.mp3', 0, CAST(N'2020-09-04T07:45:36.063' AS DateTime), N'909090', N'通过管理员账户添加', 0)
INSERT [dbo].[SongsInfo] ([id], [SongsName], [Spell], [NameLength], [SongsType], [Singer], [FileName], [playNum], [LastUpdateTime], [AdminName], [UpdateReason], [IsDelete]) VALUES (2007, N'江南', N'JIANGNA', 2, 1, 2023, N'E:\KTVMp3\江南.mp3', 0, CAST(N'2020-09-04T07:45:55.887' AS DateTime), N'909090', N'通过管理员账户添加', 0)
INSERT [dbo].[SongsInfo] ([id], [SongsName], [Spell], [NameLength], [SongsType], [Singer], [FileName], [playNum], [LastUpdateTime], [AdminName], [UpdateReason], [IsDelete]) VALUES (2008, N'明天过后', N'MINGTIANGUOHOU', 4, 1, 2019, N'E:\KTVMp3\明天过后.mp3', 0, CAST(N'2020-09-04T07:46:23.250' AS DateTime), N'909090', N'通过管理员账户添加', 0)
INSERT [dbo].[SongsInfo] ([id], [SongsName], [Spell], [NameLength], [SongsType], [Singer], [FileName], [playNum], [LastUpdateTime], [AdminName], [UpdateReason], [IsDelete]) VALUES (2009, N'十年', N'SHINIAN', 2, 1, 2013, N'E:\KTVMp3\十年.mp3', 0, CAST(N'2020-09-04T07:46:39.857' AS DateTime), N'909090', N'通过管理员账户添加', 0)
INSERT [dbo].[SongsInfo] ([id], [SongsName], [Spell], [NameLength], [SongsType], [Singer], [FileName], [playNum], [LastUpdateTime], [AdminName], [UpdateReason], [IsDelete]) VALUES (2010, N'天涯过客', N'TIANYAGUOKE', 4, 1, 2020, N'E:\KTVMp3\天涯过客.mp3', 0, CAST(N'2020-09-04T07:47:02.997' AS DateTime), N'909090', N'通过管理员账户添加', 0)
INSERT [dbo].[SongsInfo] ([id], [SongsName], [Spell], [NameLength], [SongsType], [Singer], [FileName], [playNum], [LastUpdateTime], [AdminName], [UpdateReason], [IsDelete]) VALUES (2011, N'一起giao', N'YIQIgiao', 6, 2, 2021, N'E:\KTVMp3\一起giao.mp3', 0, CAST(N'2020-09-04T07:47:21.250' AS DateTime), N'909090', N'通过管理员账户添加', 0)
INSERT [dbo].[SongsInfo] ([id], [SongsName], [Spell], [NameLength], [SongsType], [Singer], [FileName], [playNum], [LastUpdateTime], [AdminName], [UpdateReason], [IsDelete]) VALUES (2012, N'阴天快乐', N'YINTIANKUAILE', 4, 1, 2013, N'E:\KTVMp3\阴天快乐.mp3', 0, CAST(N'2020-09-04T07:47:51.163' AS DateTime), N'909090', N'通过管理员账户添加', 0)
INSERT [dbo].[SongsInfo] ([id], [SongsName], [Spell], [NameLength], [SongsType], [Singer], [FileName], [playNum], [LastUpdateTime], [AdminName], [UpdateReason], [IsDelete]) VALUES (2013, N'只是太爱你', N'ZHISHITAIAINI', 5, 1, 2022, N'E:\KTVMp3\只是太爱你.mp3', 0, CAST(N'2020-09-04T07:49:39.557' AS DateTime), N'909090', N'通过管理员账户添加', 0)
INSERT [dbo].[SongsInfo] ([id], [SongsName], [Spell], [NameLength], [SongsType], [Singer], [FileName], [playNum], [LastUpdateTime], [AdminName], [UpdateReason], [IsDelete]) VALUES (2014, N'星月神话', N'XINGYUESHENHUA', 4, 1, 2014, N'E:\KTVMp3\星月神话.mp3', 0, CAST(N'2020-09-04T07:50:18.377' AS DateTime), N'909090', N'通过管理员账户添加', 0)
INSERT [dbo].[SongsInfo] ([id], [SongsName], [Spell], [NameLength], [SongsType], [Singer], [FileName], [playNum], [LastUpdateTime], [AdminName], [UpdateReason], [IsDelete]) VALUES (2015, N'归去来兮', N'GUIQULAIXI', 4, 1, 2017, N'E:\KTVMp3\归去来兮.mp3', 0, CAST(N'2020-09-04T07:52:47.170' AS DateTime), N'909090', N'通过管理员账户添加', 0)
INSERT [dbo].[SongsInfo] ([id], [SongsName], [Spell], [NameLength], [SongsType], [Singer], [FileName], [playNum], [LastUpdateTime], [AdminName], [UpdateReason], [IsDelete]) VALUES (2016, N'泡沫', N'PAOMO', 2, 1, 2019, N'E:\KTVMp3\泡沫.mp3', 0, CAST(N'2020-09-04T07:54:19.173' AS DateTime), N'909090', N'通过管理员账户添加', 0)
INSERT [dbo].[SongsInfo] ([id], [SongsName], [Spell], [NameLength], [SongsType], [Singer], [FileName], [playNum], [LastUpdateTime], [AdminName], [UpdateReason], [IsDelete]) VALUES (3002, N'deom', N'deom', 4, 1, 2026, N'C:\Users\MSI-CN\Pictures\Saved Pictures\UPCK{5W9JHC]SL0971MKU}P.png', 0, CAST(N'2020-09-19T12:24:18.617' AS DateTime), N'909090', N'通过管理员账户添加', 1)
INSERT [dbo].[SongsInfo] ([id], [SongsName], [Spell], [NameLength], [SongsType], [Singer], [FileName], [playNum], [LastUpdateTime], [AdminName], [UpdateReason], [IsDelete]) VALUES (4002, N'测试deom3', N'CESHIdeom3', 7, 1, 2015, N'E:\KTVMp3\赵公子买单.mp3', 0, CAST(N'2020-09-19T13:58:51.057' AS DateTime), N'909090', N'通过管理员账户添加', 0)
SET IDENTITY_INSERT [dbo].[SongsInfo] OFF
SET IDENTITY_INSERT [dbo].[SongsType] ON 

INSERT [dbo].[SongsType] ([id], [TypeName], [LastUpdateTime], [AdminName], [UpdateReason], [IsDelete]) VALUES (1, N'华语', CAST(N'2020-08-22T22:19:39.010' AS DateTime), N'909090', N'通过数据库添加', 0)
INSERT [dbo].[SongsType] ([id], [TypeName], [LastUpdateTime], [AdminName], [UpdateReason], [IsDelete]) VALUES (2, N'RAP', CAST(N'2020-08-22T22:20:18.160' AS DateTime), N'909090', N'通过数据库添加', 0)
INSERT [dbo].[SongsType] ([id], [TypeName], [LastUpdateTime], [AdminName], [UpdateReason], [IsDelete]) VALUES (3, N'粤语', CAST(N'2020-08-22T22:20:18.167' AS DateTime), N'909090', N'通过数据库添加', 0)
INSERT [dbo].[SongsType] ([id], [TypeName], [LastUpdateTime], [AdminName], [UpdateReason], [IsDelete]) VALUES (4, N'欧美', CAST(N'2020-08-22T22:20:18.167' AS DateTime), N'909090', N'通过数据库添加', 0)
SET IDENTITY_INSERT [dbo].[SongsType] OFF
SET IDENTITY_INSERT [dbo].[UserInfo] ON 

INSERT [dbo].[UserInfo] ([id], [UserName], [PassWord]) VALUES (1, N'808080', N'BA0819F263287AF1FF010C5A323355')
INSERT [dbo].[UserInfo] ([id], [UserName], [PassWord]) VALUES (2, N'909090', N'E1ADC3949BA59ABBE56E057F2F883E')
SET IDENTITY_INSERT [dbo].[UserInfo] OFF
SET IDENTITY_INSERT [dbo].[UserRequest] ON 

INSERT [dbo].[UserRequest] ([id], [request], [requestTime], [isRead]) VALUES (1, N'测试', CAST(N'2020-09-11T04:58:42.483' AS DateTime), 1)
INSERT [dbo].[UserRequest] ([id], [request], [requestTime], [isRead]) VALUES (2, N'测试1', CAST(N'2020-09-11T05:06:08.293' AS DateTime), 1)
INSERT [dbo].[UserRequest] ([id], [request], [requestTime], [isRead]) VALUES (1002, N'', CAST(N'2020-09-19T10:50:30.160' AS DateTime), 1)
INSERT [dbo].[UserRequest] ([id], [request], [requestTime], [isRead]) VALUES (1003, N'??????????', CAST(N'2020-09-19T11:00:52.403' AS DateTime), 1)
INSERT [dbo].[UserRequest] ([id], [request], [requestTime], [isRead]) VALUES (1004, N'??1111111', CAST(N'2020-09-19T11:23:49.457' AS DateTime), 1)
INSERT [dbo].[UserRequest] ([id], [request], [requestTime], [isRead]) VALUES (1005, N'3号包厢一件啤酒', CAST(N'2020-09-19T11:30:58.663' AS DateTime), 1)
INSERT [dbo].[UserRequest] ([id], [request], [requestTime], [isRead]) VALUES (1006, N'四号包厢的麦坏了。', CAST(N'2020-09-19T11:32:40.920' AS DateTime), 1)
INSERT [dbo].[UserRequest] ([id], [request], [requestTime], [isRead]) VALUES (1007, N'给五号包厢上一杯卡布奇诺', CAST(N'2020-09-19T11:37:45.070' AS DateTime), 1)
INSERT [dbo].[UserRequest] ([id], [request], [requestTime], [isRead]) VALUES (1008, N'6号包厢一件啤酒', CAST(N'2020-09-19T11:42:29.540' AS DateTime), 1)
INSERT [dbo].[UserRequest] ([id], [request], [requestTime], [isRead]) VALUES (1009, N'7号包厢一件啤酒', CAST(N'2020-09-19T11:43:51.833' AS DateTime), 1)
INSERT [dbo].[UserRequest] ([id], [request], [requestTime], [isRead]) VALUES (1010, N'8号包厢的人在打架，快叫保安！！！！', CAST(N'2020-09-19T11:46:36.570' AS DateTime), 1)
INSERT [dbo].[UserRequest] ([id], [request], [requestTime], [isRead]) VALUES (1011, N'9号包厢着火了！！', CAST(N'2020-09-19T11:47:28.710' AS DateTime), 1)
INSERT [dbo].[UserRequest] ([id], [request], [requestTime], [isRead]) VALUES (1012, N'10号包厢的啤酒喝完了，再来一件~', CAST(N'2020-09-19T11:51:54.873' AS DateTime), 1)
INSERT [dbo].[UserRequest] ([id], [request], [requestTime], [isRead]) VALUES (1013, N'deom', CAST(N'2020-09-19T12:26:15.130' AS DateTime), 1)
INSERT [dbo].[UserRequest] ([id], [request], [requestTime], [isRead]) VALUES (1014, N'11111', CAST(N'2020-09-19T12:26:28.680' AS DateTime), 1)
INSERT [dbo].[UserRequest] ([id], [request], [requestTime], [isRead]) VALUES (2002, N'测试', CAST(N'2020-09-19T13:59:57.507' AS DateTime), 1)
SET IDENTITY_INSERT [dbo].[UserRequest] OFF
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0男，1女' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SingerInfo', @level2type=N'COLUMN',@level2name=N'Gender'
GO
